\begin{example}
  Let $k$ be a field of characteristic $\neq 2$.
  Let $C \subseteq \PR_k^2$ be defined by $ZY^2 = (X+Z)X(X-Z)$.
  E.g. on $D(Z)$, let $x = \frac{X}{Z}$, $y = \frac{Y}{Z}$.
  We have $C \cap D(Z) = V(y^2 = x^3 - x))$.
  
  There exists $w \in \Gamma(C, \Omega_{C/k})$ such that $\MO_C \to \Omega_{C/k}$,  $1 \mapsto w$ is an isomorphism.
  In particular ($k$ algebraically closed is not necessary, but we shall assume this), $\Gamma(C,\Omega_{C/k}) = k$.
  Let us show this.
  
  $A = k[x,y]/(y^2 = x^3 -x)$.
  $\Omega_{C \cap D(Z)/k} = (A\delta x \oplus A \delta y)/\delta f = A \delta x \oplus A \delta y / (2y\delta y = (3x^2 -1)\delta x)$.
  On $U_1 = D(y)$ we set $w_1 = \frac{\delta x}{y} \in \Gamma(U_1,\Omega_{X/k})$.
  
  On $D(y) \cap D(3x^2 -1)$ ($D(y), D(3x^2-1) \subseteq D(Z)$), we have $\frac{\delta x}{y} = \frac{1}{y}\frac{2y}{(3x^2-1)} \delta y = \frac{2}{3x^2-1} \delta y$.
  
  On $U_2 = D(3x^2-1)$ we define $w_2 = \frac{2}{3x^2-1} \delta y$.
  Then $\res{w_1}{D(y)\cap D(3x^2-1)} = \res{w_2}{D(y)\cap D(3x^2-1)}$.
  
  On $D(Y)$, let $t = \frac{X}{Y}$, $x = \frac{X}{Z} = \frac{t}{v}$, $v = \frac{Z}{Y}$, $y = \frac{Y}{Z} = \frac{1}{v}$.
  We have $D(Y) \cap C = V(v = t^3-tv^2)$, $B = k[t,v]/(v=t^3 - tv^2)$, and $\Omega_{D(Y)\cap C /k} = B \delta t \oplus B \delta v / ((1+2vt)\delta v = (3t^2-v^2)\delta t)$.
  
  On $D(Y) \cap D(Z) \cap D(1+2vt)$, we have
  \begin{align*}
    \frac{\delta x}{y} &= v \delta \left(\frac{t}{v}\right) = \delta t - \frac{t}{v} \delta v \\
                       &= \delta t - \frac{t}{v} \cdot \frac{3t^2 - v^2}{1+2vt} \delta t \\
                       &= \delta t \left(1-\frac{3tv^2 + 3v - tv^2}{v(1+2vt)}\right) \\
                       &= -\frac{2}{1 + 2vt} \; ,
  \end{align*}
  which extends to $t=v=0$. %\footnote{Mr. Oberdieck is suggesting that he may have miscalculated somewhere, after some questions from some students, but he assures that it is not zero.}

  On $U_3 = D(1 +2vt)$ we define $w_3 = -\frac{2}{1+2vt} \delta t$.
  
  Therefore $\res{w_i}{U_i \cap U_j} = \res{w_j}{U_i \cap U_j}$ for all $i,j \in \set{1,2,3}$.
  Moreover $C = U_1 \cup U_2 \cup U_3$.
  Let $w \in \Gamma(C,\Omega_{C/k})$ such that $\res{w}{U_i} = w_i$.
  By calculation, $\Omega_{C/k}$ is locally free of rank $1$.
  Moreover, $w(x) \neq 0$ in $\Omega_{C/k,x} \otimes_{\MO_X,x} \kappa (x)$ for all $x \in X$.
  Therefore $\MO_X \to \Omega_X$, $1 \mapsto w$ is an isomorphism of stalks, hence we have an isomorphism.

  The last part follows from the following fact:
  For an irreducible and reduced proper variety $X$ over an algebraically closed field $k$, we have $\Gamma(X,\MO_X) = k$.
  We see this fact as follows:

  $\Hom_{\mathrm{Sch}/k}(X,\mathbb{A}^1_k) \cong \Hom_{k-\mathrm{alg}}(k[t],\MO_X(X)) = \MO_X(X)$, $f \colon X \to \mathbb{A}^1 \mapsto f^\# (t)$.
  Let $f \colon X \to \mathbb{A}^1$.
  Since $X$ is proper and $\PR^1$ is separated, $\iota \circ f$ is proper (which follows from \Cref{properseparated}).
  We have \[\iota \circ f \colon X \to \mathbb{A}^1 \xhookrightarrow[\text{open immersion, proper}]{\iota} \PR^1\] is proper.
  $\iota \circ f (X)$ is closed in $\PR^1$, but $\neq \PR^1$, so $f(X) = \setdef{(x-a)}{a \in k}$.
  Therefore $f^\# (p) = 0$ for all $p \in X$
  Therefore $f^\# (t-a)^N = 0$ for some $N \geq 1$.
  Therefore $f^\# (t-a) = f^\#(t) - a = 0$
\end{example}

\begin{remark}
  We need algebraically closed. For $X = \Spec \C = \Spec \R[X]/(X^2+1)$, $\MO_X(X) = \C$, not $\R$.
  $X$ is a proper variety.
\end{remark}
\begin{lemma} \label{properseparated}
  Let $X \xrightarrow{f} Y \xrightarrow{g} Z$. If $g \circ f$ is proper and $g$ is separated, then $f$ is proper
\end{lemma}
\begin{proof}
  Factor $f$ as $\pr_2 \circ (\id,f)$.
\end{proof}
\begin{definition}[genus]
  Let $C$ be a normal proper curve over $k$.
  Define the \textbf{genus} of $C$ by $g(C) = \dim_k \Gamma(C,\Omega_{C/k})$.
\end{definition}
\begin{example}
  $g(C) = 1$ by our proposition,
\end{example}
\begin{example}
  $C = \PR_k^1$, $\Omega_{\PR_k^1} = \MO_{\PR^1}(-2)$ (Exercise).
  $\Gamma(\PR^1,\MO_{\PR^1}(-2)) = 0$, therefore $g (\PR^1) = 0$.
\end{example}
\begin{definition}[smooth of dimension $n$]
  A scheme $X/k$ is \textbf{smooth of dimension $n$ over $k$} if
  \begin{itemize}
  \item $X$ is locally of finite type
  \item $X$ is pure of dimension $n$, that is every irreducible component of $X$ is of dimension $n$.
  \item $\Omega_{X/k}$ is localy free of rank $n$
  \end{itemize}
\end{definition}
\begin{example}
  $X = \Spec k[x]/x^2$.
  \begin{equation*}
    \Omega_{X/k} = (k[x]/x^2)\delta x/2x\delta x = \begin{cases} k \delta x & \text{if } \operatorname{char} k \neq 2 \\ k[x]/x^2 & \text{if } \operatorname{char} k = 2\end{cases} \,
  \end{equation*}
  since $2x \delta x = \delta (x^2)$.
  So $X$ is not smooth, as the first case is not free, and the second case is free of wrong rank.

\end{example}
\begin{lemma}
  Let $K/k$ be a finite algebraic extension.
  Then $X = \Spec (K)$ is smooth if and only if $K/k$ is separable.
\end{lemma}
\textbf{Recall:} Ket $K/k$ be an algebraic extension.
Then $K/k$ is separable if and only if every $\alpha \in K$ is a simple root of its irreducible polynomial $f_\alpha$ over $k$, that is $f_\alpha^\prime \neq 0$.

\begin{proposition}
  $X = \Spec K$ is always smooth over $K$, but may be not smooth over $k$.
\end{proposition}
\begin{proof}
  For every $\alpha \in K$, $0 = \delta f_\alpha (\alpha) = f_\alpha^\prime(\alpha)\delta_\alpha$.
  If $K/k$ is separable, then $\delta_\alpha = 0$ for all $\alpha$.
  Therefore $\Omega_{K/k} = 0$.
\end{proof}
\begin{definition}[smooth scheme]
  We say that a scheme $X$ is \textbf{smooth over $k$} if every connected component is smooth of some dimension over $k$.
\end{definition}
\begin{remark}~ 
  \begin{enumerate}
  \item $\mathcal T_X = \Omega_{X/k}^\vee = \Hom_{\MO_X}(\Omega_{X/k},\MO_X)$ is the tangent sheaf.
    (If $\Omega_{X/k}$ is not locally free of finite rank, then $\Omega_{X/k}^{\vee\vee}$ may not be isomorphic to $\Omega_{X/k}$.
    Therefore $\Omega_{X/k}$ contains more information in general)
  \item The vector bundle associated to $\mathcal T_X$, $\Omega_X$ ($X$ smooth), is the tangent/cotangent bundle of $X$.
  \item Let $X$ be smooth of dimension $n$ over $k$.
    $\omega_X = \bigwedge^n \Omega_{X/k}$ is the canonical line bundle
  \end{enumerate}
\end{remark}

What is $\omega_{\PR^n}$?

\begin{example}
  $X = \Spec A = \mathbb{A}^n$, $A = \pol{k}{x}{1}{n}$.
  $\Omega_{A/k} = A \delta x_1 \oplus \dotsb \oplus A \delta x_n$, $\omega_{A/k} = A \delta x_1 \wedge \dotsb \wedge \delta x_n$.
\end{example}
\section{Regular versus Smooth}
\begin{theorem} \label{regularsmooth}
  Let $X$ be a scheme of finite type over $k$.
  \begin{enumerate}[label=(\alph*)]
  \item $X$ is smooth over $k$ $\implies$ $X$ is regular
  \item If $k$ is perfect, then $X$ is smooth over $k$ if and only if $X$ is regular
  \end{enumerate}
\end{theorem}
\begin{remark}~ 
  \begin{itemize}
  \item $K/k$ is separable if and only if there exists a transcendence
    basis $\set{x_i}_i$ such that $K/k(\set{x_i}_i)$ is a separable algebraic extension.
  \item $k$ is perfect if and only if every finitely generated field extension of $k$ is separable
  \item Every algebraically closed field and every field of characteristic $0$ is perfect.
\end{itemize}
\end{remark}
\begin{lemma}[Key Lemma]
  Let $X$ be a scheme of finite type over $k$.
  Let $p \in X$ such that $\kappa(p) = k$.
  Then $\Omega_{X/k,p} \otimes_{\MO_{X,p}} \kappa(p) = \mi / \mi^2$, where $\mi \subseteq \MO_{X,p}$ is the maximal ideal.

  Algebraic version:
  Let $B$ be a noetherian local ring such that $k \subseteq B$ and $B/\mi = k$.
  Then $\Omega_{B/k} \otimes_B k = \mi / \mi^2$.
\end{lemma}
\begin{proof} Use \Cref{exactKaehler} with $A = \mi$, $C = B/\mi = k$, $A \to B \to C$. Then
  \begin{equation*}
    \mi / \mi^2 \xrightarrow{\delta} \Omega_{B/k} \otimes_B k \to \Omega_{C/k} = \Omega_{k/k} = 0 \to o
\end{equation*}
is exact.
What is $\delta$?
\begin{equation*}
  \begin{tikzcd}
    \mi \arrow[d] \arrow[r] & B \arrow[r,"\delta"] & \Omega_{B/k} \arrow[r]&  \Omega_{B/k} \otimes_B k \\
    \mi / \mi^2 \arrow[urrr, "\delta"]
  \end{tikzcd}
\end{equation*}
Claim: $\delta$ is injective.
We check that $\delta^\ast$ is surjective.
\begin{equation*}
  \operatorname{Der}_k (B,k) \cong \Hom_B(\Omega_{B/k},k) \cong \Hom_k (\Omega_{B/k} \otimes_B k ,k) \xrightarrow{\delta^\ast} \Hom_k(\mi / \mi^2, k)
\end{equation*}
Let $h \colon \mi / \mi^2 \to k$.
Define $\phi \colon B \to k$, $b \mapsto (b-b_0)$.
Glue $a,b \in B$, $(a - a_0)(b-b_0) \in \mi$ ($b_0 =$ image of $b$ in $B/\mi = k$), so
\begin{align*}
  0 = h((a-a_0)(b-b_0)) &= h(ab-(ab)_0) - a_0 h(b-b_0) - b_0 h(a-a_0)  \\
                        &= \phi(ab) - a_0 \phi(b) - b_0 \phi(a) \; ,
\end{align*}
therefore $\phi$ is a derivation over $k$ corresponding to $g \colon \Omega_{B/k} \to k$.
We have($c \in \mi$) $\delta^\ast(g)(c) g(\delta c) = \phi(c) = h(c - c_0) = h(c)$
\end{proof}

\begin{proof}[Proof of \Cref{regularsmooth} (b) if $k$ is algebraically closed]
  Assume that $X$ is connected, and assume that $X$ is smooth of dimension $n$ over $k$.

  $X$ is pure of dimension $n$:  $\implies$ ($X$ is of finite type) that $\dim \MO_{X,p} = n$ for every closed point $p \in X$.
  
  $\Omega_{X/k}$ is locally free of rank $n$:  Let $p \in X$ be closed.
  Then $\kappa (p) = k$, as $k$ is algebraically closed.
  Therefore $\mi_p / \mi_p^2 = \Omega_{X/k,p} \otimes_{\MO_{X,p}} \kappa(p) = \kappa(p)^{\oplus n}$.

  For all $p \in X$ closed, $\dim (\mi / \mi^2) = n = \dim \MO_{X,p}$ therefore $p \in X$ is regular.

  \Cref{regularclosedpoints} implies that $X$ is regular.

  Conversely, let $X$ be regular.
  For all $p \in X$, $\MO_{X,p}$ is regular, therefore integral.
  Therefore $X$ is integral of some dimension $D$.

  Assume $X = \Spec \pol{k}{x}{1}{n}/(f_1,\dotsc,f_r) = \Spec A$.

  $\tilde I / \tilde I^2 \xrightarrow{\delta} \Omega_{\mathbb{A}^n/X} \to \Omega_{X/k} \to 0$, $I = (f_1,\dotsc,f_r)$.

  \begin{equation*}
    \begin{tikzcd}
      f_i & I / I^2 \arrow[r,"\delta"] & A \delta x_1 \oplus \dotsb \oplus A \delta x_n \arrow[r] & \Omega_{A/k} \arrow[r] & 0 \\
      e_i \arrow[u, mapsto] & A^{\oplus r} \arrow[u] \arrow[ur, "J_{(f_1,\dotsc,f_r)} = J \text{ (jacobian)}"']
    \end{tikzcd}
  \end{equation*}
  By the Jacobi criterion, for all $p \in X$ closed, $\operatorname{rk} J_p = n - \dim (\MO_{X,p}) = n-D$.

    $V((n-D)\times(n-D) \text{ minors of } J)$, $D((n-D)\times(n-D) \text{ minors of } J)$ contain no closed points, hence are empty.
    Therefore $\operatorname{rk}(J_p) = n-D$ for all $p \in X$.

    $\dim (\Omega_{X/k} \otimes_{\MO_{X,p}} \kappa (p)) = n - \operatorname{rk} J_p = D$, which is independent of $p$, therefore constant.
    So $\Omega_{X/k}$ is locally free o rank $D$.
\end{proof}
\begin{remark}
  In general:
  
  $X$ smooth over $k$ over $k$ $\iff$ $X \times_k \bar k$ is smooth over $\bar k$

  If $k$ is perfect, then $X$ is regular $\iff$ $X \times_k \bar k$ is regular.
\end{remark}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
