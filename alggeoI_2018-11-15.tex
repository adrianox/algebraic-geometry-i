\begin{corollary}
	Let $f \colon X \to Y$ be a morphism. 
	\begin{enumerate}[label=(\alph*)]
		\item Let $x \in X$. Let \[e(x)=\max\setdef{\dim Z}{Z \text{ irred. comp. of }f^{-1}(f(x)) \text{ s.t. }x \in Z}\]
		%[some picuter of this]
		Then, $e(x)$ is upper semi-continuous, i.e. $e^{-1}([n, \infty))$ is closed for all $n$. 
		\item If $f$ is closed, then \[\tilde e(y)=\max\setdef{\dim Z}{Z \subset f^{-1}(y) \text{ irred. comp.}}\] is upper semi-continuous. 
	\end{enumerate}
	In words: Fiber dimensions jump up on closed subsets. 
\end{corollary}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
		\item By induction on $\dim Y$. 
		
		If $\dim Y = 0$, there is nothing to prove. 
		
		In general, let $r=\dim X - \dim Y$. 
		If $n \le r$, then $\setdef{x \in X}{e(x) \ge n}=X$ by \Cref{fd>=r}. 
		If $n > r$, let $U$ be given as in \Cref{fibdim}. Then \[\setdef{x \in X}{e(x) \ge n} \subset f^{-1}(U)^C \,.\] Let's restrict $f$ to $f^{-1}(U)^C$. 
		Write $f^{-1}(U)^C = X_1 \cup \dots \cup X_l$ as a union of irreducible components. 
		We have $U^c = Y_1 \cup \dots \cup Y_l$ (possibly with repetitions). 
		Let $f_i=\res{f}{X_i} \colon X_i \to Y_i$. 
		Now, \[\setdef{x \in X}{e(x) \ge n}=\bigcup_i \setdef{x \in X_i}{e_{f_i}(x) \ge n}\] which is closed by induction. 
		\item We know that $$\setdef{y \in Y}{\tilde e(y) \ge n}=f\left( \setdef{x \in X}{e(x) \ge n}\right)$$ is closed. Therefore, $\setdef{x \in X}{e(x) \ge n}$ is closed. \qedhere
	\end{enumerate}
\end{proof}
\begin{definition}[constructible] 
	A subset $A \subset X$ is called \textbf{constructible} if $A=\bigsqcup_{i = 1}^m A_i$ where $A_i$ are locally closed. 
	
	The set of constructible subsets in $X$ is the smallest set of subset of $X$ which contain the opens and is closed under finite $\bigcup$, finite $\bigcap, ( \; )^C$
\end{definition}
\begin{corollary}[Chevalley's theorem] \label{chevalley}
	Let $f \colon X \to Y$ be a morphism. The the image $f(X)$ is a constructible subset of $Y$. More generally, the image of constructible is constructible. 
\end{corollary}
\begin{proof}
	By induction on $\dim Y$. 
	If $f$ is not dominant, replace $Y$ by $\overline{f(X)}$ and apply the induction.
	
	If $f$ is dominant, we have $\emptyset \neq U \subset f(X)$. Therefore, $f(X)=U \sqcup (U^C \cap f(X))$. Replace $(X,Y,f)$ by $(f^{-1}(U^C),U^C,\res{f}{f^{-1}(U^C)})$. 
\end{proof}
\chapter{Affine schemes / $\Spec R$}
\section{The Zariski Topology on $\Spec R$}
Recall the correspondences given in \Cref{VI} and \Cref{VI2} for an algebraically closed field $k$.
What if $k$ is not algebraically closed ($\R, \Q, \Z/p$)? These correspondences do not hold anymore. 
\begin{definition}[spectrum]
	Let $R$ be a commutative ring with $1$. Let \[\Spec(R)=\setdef{\pid \subset R}{\pid \text{ prime ideal}}\] be the \textbf{spectrum of $R$}.
\end{definition}
We will build a correspondence to the vanishing set which was defined for an ideal $\ai \subset k[x_1, \dots , x_n]$ as \[V(\ai)=\setdef{(a_1, \dots , a_n) \in k^n}{\forall f \in \ai \colon f(a_1, \dots , a_n)=0} \, .\]
Let $\mi = (x_1-a_1, \dots , x_n-a_n)$.
Therefore, $f(a_1, \dots , a_n)$ is the image of $f$ under 
\begin{align*}
	\varphi \colon k[x_1, \dots , x_n] & \longrightarrow k[x_1, \dots , x_n]/\mi \\
	x_i & \longmapsto a_i \\
	f & \longmapsto f(a_1, \dots , a_n)
\end{align*} 
We have $f(a_1, \dots , a_n)=0 \iff f \subset \mi$. 
\begin{definition}[vanishing set] Let $\ai \subset R$. Define \[V(\ai)=\setdef{\pid \in \Spec R}{\ai \subset \pid} \subset \Spec R\,\] the \textbf{vanishing set of $\ai$}.
\end{definition}
\begin{proposition}[Zariski topology]
	The set $\mathcal C=\setdef{V(\ai)}{\ai \subset R \text{ ideal}}$ is the collection of closed subsets of a topology on $\Spec R$ called the \textbf{Zariski topology}.
\end{proposition}
\begin{proof}
	We need to check that $\mathcal C$ is 
	\begin{enumerate}
		\item closed under finite union
		\item closed under arbitrary intersection
		\item $\emptyset, \Spec R \in \mathcal C$
	\end{enumerate}
	We check that
	\begin{enumerate}
		\item We have $V(\ai)\cup V(\bi)=\setdef{\pid \in \Spec R}{\ai \subset \pid \vee \bi \subset \pid}=V(\ai \bi)$: If $\ai \subset \pid$ or $\bi \subset \pid$ we have $\ai \bi \subset \pid$. 
		
		Conversly, if $\ai \bi \subset \pid$ but $\ai \notin \pid$, let $s \in \ai \setminus \pid$, then $s \cdot t \in \pid$ for all $t \in \pid$ hence $t \in \bi$. This implies $\bi \subset \pid$.
		\item $\begin{aligned}
			\bigcap_{i \in I} V(\ai_i) = \setdef{\pid}{\ai_i \subset \pid \text{ for all }i} = \setdef{\pid}{\sum_{i \in I}\ai_i \subset \pid} = V\left( \sum \ai_i \right)
		\end{aligned}$
		\item $V(R)=\emptyset$, $V(0)=R$. \qedhere
	\end{enumerate}
\end{proof}
\begin{definition}[value] Let $f \in R, \pid \in \Spec R$. The \textbf{value of $f$ at $\pid$} is defined by: 
\begin{itemize}
	\item If $\pid = \mi$ is maximal, \[f(\pid) \coloneqq \text{Image of $f$ under } R \to R/\pid \, .\]
	\item More generally ($\pid$ prime), \[f(\pid) \coloneqq \text{Image of $f$ under } R \to R/\pid \to \Frac(R/\pid)\]
\end{itemize}
\end{definition}
\begin{remark}[residue field]
	Let $R \supset \pid$ and $R \setminus \pid = S$. Then $S^{-1}R=R_\pid$ is a local ring with maximal ideal $\pid \cdot R_\pid$. We have $R_\pid / (\pid \cdot R_\pid)=\Frac(R / \pid) \eqqcolon \kappa(\pid)$ which is called the \textbf{residue field at $\pid$}.
\end{remark}
\begin{lemma}
	We have \[V((f))=\setdef{\pid \in \Spec R}{f(\pid)=0} \,.\]
\end{lemma}
\begin{proof}
	Compute \[V((f))=\setdef{\pid \in \Spec R}{f \in \pid}=\setdef{\pid}{f(\pid)=0} \qedhere\]
\end{proof}
\begin{definition}[basic open subsets]
	Let \[D(f)=\setdef{\pid \in \Spec R}{f(\pid) \neq 0} \, ,\] a \textbf{basic open subset}. 
\end{definition}
\begin{lemma}
	$\setdef{D(f)}{f \in R}$ is a basis of Zariski topology. 
\end{lemma}
\begin{proof}
	Let $U \subset \Spec R$ be open. We need to show that there exists $f_i, i \in I$ such that $U=\bigcup_{i \in I} D(f_i)$. We have $$U^C = V(\ai) = \setdef{\pid}{\ai \subset \pid}=\setdef{\pid}{f(\pid)=0 \text{ for all }f \in \ai}$$
	Therefore, \[  U=\setdef{\pid}{\exists f \in \ai \text{ s.t. } f(\pid)\neq 0}=\bigcup_{f \in \ai} D(f) \] which shows that $U$ can be written as a union of elements from the basis.
\end{proof}
\begin{theorem}
	There is a one-to-one-correspondence\[\begin{tikzcd} \{  \text{closed subsets in } \Spec R \} \arrow[yshift=4]{rr}{I} & & \{ \text{radical ideals in } R \} \arrow[yshift=-4]{ll}{V} \end{tikzcd}\]
	where \[I(X)=\setdef{f \in R}{f(\pid)=0 \text{ for all } \pid \in X} \,.\]
\end{theorem}
\begin{proof}
	We know that $V$ is surjective (by definition of Zariski topology)- Therefore, we only need to check that $I(V(\ai))=\sqrt{\ai}$ for any ideal $\ai \subset R$. We compute 
	\begin{align*}
		I(V(\ai)) & = \setdef{f \in R}{f(\pid)=0 \text{ for all } \pid \in V(\ai)} \\
		& = \setdef{f \in R}{f \in \pid \text{ for all primes } \pid \text{ s.t. } \ai \subset \pid} \\
		& = \bigcap_{\substack{\ai \subset \pid \\ \pid \text{ prime}}} \pid 
	\end{align*}
	We claim that this is equal to $\sqrt{\ai}$.
	
	\textit{Proof.} Quotient out $\ai$, so we can assume $\ai =0$. We need to show that \[\bigcap_{\substack{\pid \subset R \\ \pid \text{ prime}}} \pid = \sqrt{(0)} = \Nil(R) \,.\]
	
	If $x \in \Nil(R)$, then $x^n = 0 \in \pid$ for all primes $\pid$. 
	
	Conversly, if $x \notin \Nil(R)$, then $R_x \neq 0$. Therefore, there exists a prime $\tilde p \subset R_x$. Then $\varphi^{-1}(\tilde \pid) \subset R$ is prime where $\varphi \colon R \to R_x$. Furthermore, $x \notin \varphi^{-1}(\tilde \pid)$. 
\end{proof}
\begin{note}
	This fails if we had defined $\Spec R$ to be the set of maximal ideals. In general, we have \[\bigcap_{\substack{\mi \subset R \\ \mi \text{ maximal}}}=\Jac(R) \supset \Nil(R) \,.\]
\end{note}
\begin{definition}[closed, generic point]
	An element $\pid \in \Spec R$ is \textbf{closed} if $\{ \pid \}$ is closed in $\Spec R$. Furthermore, $Z=\overline{\{\pid\}}$ is irreducible. We call $\pid$ a \textbf{generic point of $Z$}.
\end{definition}
\begin{example}~ \vspace{-\topsep}
	\begin{enumerate}
		\item Let $R = \C[x]$. We have $\Spec R = \setdef{(x-a)}{a \in \C} \cup \{(0)\}$. 
		Furthermore, for $0 \neq f \in \C[x]$ we have \[V(f)=\setdef{\pid \subset \C[x]}{f \in \pid} = \setdef{(x-a)}{f(a)=0}\] and therefore \[D(f)=\setdef{(x-a)}{f(a) \neq 0} \cup \{(0)\}\,.\]
		Here we have that $\{(x-a)\}=V(x-a)$ and therefore $\{(x-a)\}$ is closed. Furthermore, $\overline{\{(0)\}}=\bigcap_{f \in R, (0) \in V(f)} V(f) = V(0)= \Spec R$. 
		
		Therefore, $\{(0)\}$ is generic point of $\Spec \C[x]$. 
		\item Let $R = \R[x]$. Then, \[\Spec R[x] = \setdef{(x-a)}{a \in \R} \cup \setdef{(f)}{f \in \R[x] \text{ irred., } \deg=2 } \cup \{(0)\} \, .\]
		\begin{figure}[h]
			\centering
			\begin{tikzpicture}[scale=3.0]
				\draw (-0.5,0) -- (1.5,0) node[right] {$\R$};
				\fill (0,0) circle (0.02) node[below] (c) {$(x)$};
				\fill (0.4,0) circle (0.02) node[below] {$(x-1)$};
				\fill (1.1,0) circle (0.02) node[below] {$(x-a)$}; 
				\fill (-0.3,0.4) circle (0.02) node[below] (b) {$(f_1)$}; 
				\fill (0.2,0.6) circle (0.02) node[below] {$(f_2)$}; 
				\fill (1.9,0.5) circle (0.04) node[above right] {$(0)$};
				\node at (-0.8,-0.2) (a) {closed points};
				\draw[->] (a) -- (b);
				\draw[->] (a) -- (c);
			\end{tikzpicture}
			\caption{$\Spec \R[x]$}
		\end{figure}
		Again, $(0)$ is a generic point. 
	\end{enumerate}
\end{example}
\begin{lemma}
	Let $\varphi \colon R \to S$ be a ring homomorphism. Let $\pid \subset S$ be prime. Then $\varphi^{-1}(\pid)$ is a prime ideal.
\end{lemma}
\begin{definition}
	Let $\varphi$ be a above. Define
	\begin{align*}
		\varphi^* \colon \Spec S & \longrightarrow \Spec R \\
		\pid & \longmapsto \varphi^{-1}(\pid)
	\end{align*}
\end{definition}
\begin{example}
	Consider $\varphi \colon \R[x] \to \C[X]$ which is given by the inclusion. We have \[\varphi^*((0))=\varphi^{-1}((0))=(0)\] (the image of the generic point is the generic point).
	
	Furthermore, $\varphi^*((x-a))=\varphi^{-1}((x-a))= \begin{cases}
	(x-a), & \text{ if } a \in R \\ (x-a)(x- \overline a), & \text{ if } x \in \C \setminus R
	\end{cases}$
\end{example}
\begin{example}
	Let $R=\Z$. We have $\Spec \Z = \setdef{(p)}{p \text{ prime}} \cup \{(0)\}$. 
	%[picture]
	
	Let's compute values of $f=n \in \Z$. For $\pid=(0)$, we have \[f(\pid)=\text{Image of $n$ in }\Frac{\Z/(0)}=\Q \,.\]
	
	If $\pid = (p)$, then \[f(\pid)=\text{Image of $n$ in }\Frac{\Z/(p)}=\Z/p\]
	
	For example, if $n=10$, then $f(2)=f(5)=0$, but $f(3)=1$ and $f(7)=3$. 
	Furthermore, we have $V(10)=\{(2),(5)\}$ or more generally \[V(n)=\setdef{(p)}{n \equiv 0 \text{ in }\Z/p}=\setdef{(p)}{p|n} \, .\]
	In particular, the closed points are $\{(p)\}$ where $p$ is prime and \[\overline{\{(0)\}}=\bigcap_{\substack{f \in R \\ (0) \in V(f)}} V(f)=\Spec R\] which implies that $\{(0)\}$ is a generic point. 
	\begin{figure}[h]
	\centering
	\begin{tikzpicture}
	\draw (1,0) -- (8,0) node[right] {$\Z$};
	\fill (2,0) circle (0.1) node[below] (b) {$(2)$};
	\fill (3,0) circle (0.1) node[below] {$(3)$};
	\fill (5,0) circle (0.1) node[below] (c) {$(5)$};
	\fill (7,0) circle (0.1) node[below] {$(7)$};
	\node at (3.5,-1.5) (a) {$V(10)$};
	\draw[->] (a) -- (b);
	\draw[->] (a) -- (c);
	\fill (10,0) circle (0.15) node[below] {$(0)$} node[above]{generic point};
	\end{tikzpicture}
	\caption{$\Spec \Z$}
	\end{figure}
\end{example}
\begin{example}
	Let $R=\C[x]_{(x)}$. Then $\Spec R = \{(0),(x)\}$ and $V((x))=\{(x)\}$ and $V((0))=\Spec R$. 
	\begin{figure}[h]
		\centering
		\begin{tikzpicture}[scale=3.0]
			\fill (0,0.5) circle (0.02) node[above] {$(0)$};
			\fill (0,0) circle (0.02) node[below] {$(x)$} ;
			\draw (-1,0) -- (1,0) node[right] {$\C$};
			\draw[color=blue] (0.1,0.4) circle (0.6);
			\node[color=blue, right] at (0.7, 0.5)  {open};
			\draw[color=blue] (0.1,0.5) circle (0.3);
			\node[color=blue, above] at (0.1,0.2) {open};
		\end{tikzpicture}
		\caption{$\Spec\C[x]_{(x)}$ with nonempty open sets} \label{picC[x]_{(x)}}
	\end{figure}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
