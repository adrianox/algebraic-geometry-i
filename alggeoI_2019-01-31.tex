\section{Blow-up}
Let $X$ singular/$k$. Can we find a desingularization of $X$, i.e. proper rational morphism $\pi \colon X^\prime \to X$ such that $X^\prime$ is non-singular. 

If  $\cha(k)=0$, yes (Hironaka). If $\cha(k) \neq 0$, this is an open question. 

\begin{example}
	Consider polar coordinated on $\R^2$, $(x,y)=(r \cos \varphi,r \sin \varphi)$. 
	
	Classical domain: $\R_{\ge 0} \times \R/2 \pi Z \to \R^2$.
	
	Modified domain: $(\R \times \R)/\sim \to \R^2$ where $(r, \varphi) \sim (-r, \varphi + \pi)$. 
	%[picture]
	
	Now, $\res{\pi}{\{r \neq 0\}}$ becomes an isomorphism onto $\R^2 \setminus 0$ and \begin{align*}
		\pi^{-1}(0)&=\{ r=0\}=\R/\sim = \{ \text{angles of line through origin up to } \pi \}\\&=\{\text{line trough origin} \}=\R \PR^1
	\end{align*}
	
	Consider the singular curve $C=V(y^2-x^3-x^2)$. Write $\pi^{-1}(C)=\tilde C \cup \{ r=0 \}$. In coordinates 
	\begin{align*}
		y^2 &= x^3+x^2 \\
		r^2 \sin^2 \varpi &= r^3 \cos^3 \varphi + r^2 \cos^2 \varphi \\
		r^2(r \cos^3 \varphi + \cos^2 \varphi - \sin^2 \varphi)&=0
	\end{align*}
	Where the first factor corresponds to $r=0$, the second one to $\tilde C$. 
	
	Another example is $C_2=V(x^2-x^3)$. Here we have 
	\begin{align*}
		y^2 &= x^3 \\
		r^2 \sin^2 \varphi &= r^3 \cos^3 \varphi \\
		r^2 (r \cos^3 \varphi - \sin^2 \varphi) & = 0 
	\end{align*}
	Here for $\tilde C_2$, we have $r=\frac{\sin^2 \varphi}{\cos^3 \varphi} \approx \frac{\varphi^2}{1}$ near $\varphi= 0$. Hence, $\tilde C_2$ is non-singular. 
\end{example}
\begin{definition}[blowup of $\A^2$ at $0$]
	Let $\Bl_0\A^2 = V(xv-yu) \subset \underset{u,v}{\A^2} \times \underset{u,v}{\PR_1}$. The projection to the first factor \[\pi \colon \Bl_0\A^2 \to \A^2 \] is called the \textbf{blowup of $\A^2$ at $0$}.
\end{definition}
\begin{remark}
	The projection to the second factor is $\mathbb V (\MO_{\PR^2}(-1)) \to \PR^1$ of $\Bl_0\A^2$. 
\end{remark}
\begin{remark}
	We have $\pi^{-1}(0) \cong \PR^1 = E$ the exceptional divisor. 
\end{remark}
\begin{lemma}
	$\res{\pi}{\pi^{-1}(\A^2 \setminus \{0 \})}$ is an isomorphism onto $\A^2 \setminus \{0\}$ with inverse $(\id, p)$. In particular, $\Bl_0 \A^2$ is the clousre of the graph of $p$. 
	
	Here, 
	\begin{align*}
		p \colon  \qquad \A^2 \setminus \{0 \} & \longrightarrow \PR^1 \\
		(x,y) & \longmapsto [x,y]
	\end{align*}
	As a morphism of schemes. Let $f \in k[x,y]$ be homogeneous. Then \[\res{p}{D_{\A^2}(f)} \colon D_{\A^2}(f) = \Spec (A_f) \to D_{\PR^1}(f) = \Spec (A_f)_0 \] is given by the inclusion $(A_f)_0 \hookrightarrow A_f$. 
\end{lemma}
\begin{proof}
	We compute $\pi^{-1}(D(x)) = V(xv-yu) \subset D(x) \times \PR^1$ which is equal to $V(v/u-y/x) \subset D(x) \times D(u)$. This is the graph of $D(x) = \Spec A_x  \to D(u) = \Spec (k[u,v]_u)_0 = k[v/u]$ given by $y/x \mapsto v/u$ which is the graph of $\operatorname{Graph}(\res{p}{D(x)})$. Therefore, $\pi^{-1}(\A^2 \setminus \{ 0 \}) = \operatorname{Graph}(p)$. 
\end{proof}
\begin{definition}[proper transform]
	Let $C \subset \A^2$ be a curve. Define \[\tilde C \coloneqq \overline{\pi^{-1}(C \setminus \{0\})} \] the \textbf{proper transform of $C$}.
\end{definition}
\begin{example}
	Let $C=V(y^2-x^3-x^2)$. On $D(u)$ let $V=v/u$. Then $xv = yu \iff y = xV$. The equation of $\pi^{-1}(C) \cap D(u)$ becomes 
	\begin{align*}
		&&x^2V^2 &= x^3+x^2 \\
		\iff && x^2 (V^2-x-1)=0
	\end{align*}
	Hence, $\tilde C \cap D(u) = V(v^2-x-1)$ is non-singular. 
	
	On $D(v)$ let $U=u/v$. Here $xv-yu = 0 \iff x = Uy$. On $\pi^{-1}(C) \cap C(v)$ we get \begin{align*}
		&& y^2 & = y^3U^3 + y^2U^2 \\
		\iff & & y^2(yU^3+U^2-1) & = 0
	\end{align*}
	So, $\tilde C \cap D(v)$ is also non-singular.
	
	All in all, $\tilde C$ is non-singular. 
\end{example}
\begin{example}
	Consider $y^2=x^3$. On $D(u)$ we get 
	\begin{align*}
		&& x^2C^2 &= x^3 \\
		\iff && x^2(x-V^2)& = 0
	\end{align*}
	ON $D(v)$ we get 
	\begin{align*}
		&& y^2 &= U^3y^3 \\
		\iff && y^2(U^3y-1) &= 0
	\end{align*}
	All in all, $\tilde C$ is again non-singular. 
\end{example}
\begin{definition}[blowup of $\A^n$ at $0$]
	The \textbf{blowup of $A^n$ at $0$} is \[ \Bl_0 \A^n = V(x_iy_j-x_jy_i) \subset \underset{x_1, \dots, x_n}{A^n} \times \underset{y_1, \dots , y_n}{\PR^n} \longleftrightarrow \A^n \,. \]
\end{definition}
\begin{remark}~ \vspace{-\topsep}
	\begin{itemize}
		\item $\pi^{-1}(0) = \PR^{n-1} = E $
		\item $\pi^{-1}(\A^n \setminus \{0 \}) = \operatorname{Graph(p \colon \A^n \setminus \{ 0 \} \to \PR^{n-1})}$
		\item On local charts $D(y_i) = \Spec k\left[x_1, \frac{y_1}{y_i}, \dots , \frac{y_n}{y_i} \right]$ using $x_iy_j = x_jy_i$ $\implies$ $x_i \frac{y_j}{y_i} = x_j$. 
		\item Let $X=V(I) \subset \A^n$ closed reduced subscheme. $\tilde X = \overline{\pi^{-1}(X \setminus \{0 \}})$. 
	\end{itemize}
\end{remark}
\begin{example}
	Let $X=\Spec k[x,y,z]/(x^2+y^2+z^2)$ which is singular at the origin. %[picture]
	Let $[u,v,w]$ homogeneous coordinates on $\PR^2$. On $D(u)$. Let $V=v/u$, $W=w/u$. Here $xw=zu$ and $xv = yu$. Hence, $z=xW$ and $y=xV$. On $\pi^{-1}(X) \cap D(u)$ The equation $x^2+y^2+z^2=0$ becomes $x^2+x^2V^2+x^2W^2=0$ or $x^2(1+V^2+W^2) = 0$ which is a non-singular hypersurface in $\A^3$. 
	
	By symmetry, $\tilde X \cap D(v)$ and $\tilde X \cap D(w)$ are non-singular. Furthermore, $\tilde X = E = V(u^2+v^2+w^2) \subset \PR^2$. 
	
	What is the normal bundle of $E$ in $\tilde X \cap E = E_X$ inside $\tilde X$. Here, the normal bundle is $N_{E_x} = T_{\tilde X/E_X}/T_{E_x}$
\end{example}
\section{Du Val Singularities}
\begin{definition}[quotient of a group action]
	Let $G$ be a finite group acting on $X = \Spec A$ (equivalently, $G$ acts on $A$). The quotient of $X$ by $G$ by \[X/G = \Spec (A^G)\]
\end{definition}
Let $G \subset \operatorname{SL}_2(\C)$. Let $X=\A^2 \setminus G$. Let $Y$ be the minimal resolution of singularities of $X$ obtained by repeatedly blowing up the singular points. 
	
\textbf{Claim 1}. $\pi^{-1}(0)=C_1 \cup \dots \cup C_n$ where $C_i \cong \PR^1$. 

\textbf{Claim 2}. (Theorem McKay) The dual graph of $\pi^{-1}(0)$ is isomorphic to the McKay Graph of $G$ that is the graph with vertices $\rho_1, \dots , \rho_n$ non-trivial irreducible representations of $G$ and there is an edge between $\rho_i$ and $\rho_j$ if $\rho_i \subset \rho_j \otimes \C^2$ (with standard representation on $\C^2$). 
\begin{example}
	Let $G=\langle \begin{pmatrix}
 	-1 & 0 \\
 	0 & -1
	\end{pmatrix} \rangle = \Z_2$
	with $\xi.(x,y) = (-x,-y)$. Here, $\C[x,y] = \C[x^2,xy,y^2]=\C[z_0,z_1,z_2]/(z_1^2-z_0z_2)$ which is a previous example (up to variable change). 
	
	The dual graph of $\pi^{-1}(0)$ is one point, namely $X_E = \PR^1$ and the McKay graph is one point, namely $\C$ wich an action $\zeta.e = e$. 
\end{example}
\begin{example}
	View $\C^2$ as quaternions, let $Q = \{ \pm 1, \pm i , \pm j \pm k \}$ where $i \mapsto \begin{pmatrix}
		i & 0 \\
		0 & -i
	\end{pmatrix}$
	So, $\C[x,y] = \C[(x^4-y^4)xy,x^4+y^4,x^2y^2]$ with $X=[(x^4-y^4)xy$, $Y=x^4+y^4$ and $Z=x^2y^2$, we have $X=V(X^2-X^2Z+YZ^3) \subset \A^3$. 
	\begin{comment}After change of variables, $X=V(x^2+y^2+z^2)$. 
	
	We compute $\tilde X$. on $D(u)$, let $V=v/u$, $W=w/u$. Hence, $y=xV$, $z=xW$. Here 
	\begin{align*}
		& & x^3 + x^3V^3 + x^3W^3 & = 0 \\
		\iff & & X^3(1+V^3+W^3)&=0
	\end{align*}
	Let $X_1 = V(1+V^3+W^3)$. It is singular if $3V^2=3W^2=1+V^3+W^3=0$ has a solution which is not the case.
	\end{comment} 
	We compute the blowup of $X^2-Y^2Z+YZ^3=0$. On $D(u)$, let $V=v/u$, $W=w/u$. Hence, $y=xV$, $z=xW$. Here 
	\begin{align*}
		&& x^2- x^2V^2 x W + 4x^3W^3 & = 0 \\
		\iff & & x^2(1-xV^2W+4xW^3) & = 0
	\end{align*}
	Let $X_1=V(1-xV^2W+4xW^3)$. It is singular if $V^2w+4w^3=2xVW=12xW^2-xV^2=1-xV^2W+4xW^3=0$ has a solution.\footnote{Unfortunately, Oberdieck failed this computation and had to leave.}
	
	The result after a correct computation is that $\pi^{-1}(0)=C_1 \cup C_2 \cup C_3 \cup C_4$ and the graph is that two points are connected if and only if one of them is $C_1$. The McKay graph is the isomorphic to this one. The representation are given by $Q/\pm 1 = \Z_2 \times \Z_2$ which $3$ non-trivial representations. 
\end{example}

