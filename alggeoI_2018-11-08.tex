\begin{proposition} \label{finiteMor}
  Let $f \colon X \to Y$ be finite.
  Then we have
  \begin{enumerate}
  \item
    $f$ is closed
  \item
    For all $y \in Y$, the set $f^{-1}(y)$ is finite.
  \item
    $f$ surjective $\iff$ $f^\ast$ injective
  \item
    $f$ surjective $\implies$ $\dim X = \dim Y$
  \end{enumerate}
\end{proposition}
\begin{proof}~ \vspace{-\topsep}
  \begin{enumerate}
  \item Let
    $\ai \subseteq k[x_1,\dotsc,x_n]$
    \begin{equation*}
      \begin{tikzcd}
        \quad k^n  \supseteq X \arrow[d]\supseteq V(\ai) & \ni p \arrow[r, mapsfrom] & \text{maximal ideal } \mi \supseteq \ai  \\
        Y& \ni f(p) \arrow[r, mapsto] &  \text{maximal ideal } (f^\ast)^{-1} (\mi) \supseteq (f^\ast)^{-1}(\ai)
      \end{tikzcd}
    \end{equation*}
%    We have $f(V(\ai)) \subseteq V((f^\ast)^{-1}(\ai))$, but we also have ``$\supseteq$'' since $f$ is finite and we can use going up theorem.
% proof inserted from lecture 11/5
	Let $\ai^\prime = (f^*)^{-1}(\ai)$. We will show that $f(V(\ai))=V(\ai^\prime)$.
	
	Let $\mi \subset k[X]$ be a maximal ideal such that $\mi \supset \ai$.
	But this leads to $(f^*)^{-1}(\mi) \supset (f^*)^{-1}(\ai)$. 
	All in all, $f(V(\ai)) \subset V(\ai^\prime)$. 
	
	Conversely, let $\tilde \mi \subset k[Y]$ be maximal ideal that contains $\ai^\prime$.
	Consider $k[Y]/\ai^\prime \hookrightarrow k[X]/\ai$. By the Going Up Theorem $\tilde \mi/\ai^\prime = \mi^\prime \cap k[Y]/\ai^\prime$ for some $\mi^\prime \subset k[X]/\ai$.
	
	This leads to $\tilde \mi = (f^*)^{-1}(\mi)$ where $\mi = \mi^\prime + \ai \subset k[X]$ which finally also proves $V(\ai^\prime) \subset f(V(\ai))$.
  \item
    Let $Q \in Y$ be the point corresponding to a maximal ideal $\mi \subseteq k[Y]$.
    Points of $f^{-1}(Q)$ correspond to maximal ideals in $k[X]$, and we have $(f^\ast)^{-1}(\mi) = \tilde \mi$.
    We therefore have $f^\ast(\tilde \mi) \subseteq \mi$.

    Therefore, points $P \in f^{-1}(Q)$ correspond to maximal ideals in $k[X]/f^\ast(\mi)k[X]$.
    But $k[X]/f^\ast(\mi)k[X]$ is finite dimensional over $k$, which implies that there are finitely many maximal ideals, as $k = k[Y]/\mi \subseteq k[X]/f^\ast(\mi)k[X]$ is integral. 
  \item
    $f$ surjective implies $f^\ast$ is injective (exercise).

    Conversely, consider $X = V(0)$.
    By what we showed in (a), $f(X) = V((f^\ast)^{-1}(0)) = V(0) = Y$.
  \item
    As $k[Y] \subseteq k[X]$ is integral and $k(Y) \subseteq k(X)$ is algebraic, we have $\operatorname{tr}\deg k(X) = \operatorname{tr}\deg k(Y)$. \qedhere
  \end{enumerate}
\end{proof}
\begin{definition}[norm]
	Let $L/K$ be a finite dimensional field extension. Let $f \in L$, then $m_f \colon L \to L$, $y \mapsto fy$ which is $k$-linear, we define the \textbf{norm of $f$} as \[\operatorname{Norm}_{L/K} (f) \coloneqq \det (m_f) \in K \, \]
\end{definition}
\begin{proof}[Proof of \Cref{Krull}] $ $ 
	
  \textbf{Step 1:} Reduction to the case where $X$ is affine.
	 
  We have $\dim X = \dim U$ and $\dim Z = \dim Z \cap U$ for affine $U \subseteq X$ and arbitrary $Z$.
  We can therefore assume that $X$ is affine.

  \textbf{Step 2:} Reduction to case $V(f) = Z$.
  
  Let $V(f) = Z_1 \cup \dotsb \cup Z_r$, write $Z_1 = Z$.
  Let $f_i \in k[X]$ that vanishes on $Z_i$, $i \geq 2$, but not on $Z = Z_1$.
  We restrict to $D(f_2\dotsb f_r)$.
  We have $Z \cap D(f_2\dotsb f_r) = V(f) \cap D(f_2 \dotsb f_r) = V(\res{f}{D(f_2\dotsb f_r)})$.
  Replace $X$ by $D(f_2\dotsb f_r)$ and $f$ by $\res{f}{D(f_2\dotsb f_r)}$.

  \textbf{Step 3:} Reduction to the case $X = \mathbb A^n$.
  
  By \Cref{NN}, there exist $x_1,\dotsc,x_n \in k[X]$ such that $k[X]$ is integrally dependent on $\pol{k}{x}{1}{n}$.
  This is equivalent to:
  There exists a finite $\pi \colon X \to \mathbb A^n$, $X \supseteq Z = V(\pid)$, $\pid = \sqrt{(f)}$, and $\res{\pi}{Z}$ is finite.
  We have $\pi(Z) = V((\pi^\ast)^{-1}(\pid)) = V(\pid \cap \pol{k}{x}{1}{n})$ (first equality by \Cref{finiteMor}).
  Since $\pi$ is finite, $\dim X = \dim \mathbb A^n$.
  Since $\res{\pi}{Z}$ is finite, we have $\dim{Z} = \dim{\pi(Z)}$.
  We need to show that $\pi(Z)$ is cut out by a single equation.
  Idea:
  \begin{equation*}
  \begin{tikzcd}
    k(X) \arrow[Supseteq]{r} \arrow[Supseteq]{d} & k[X] \arrow[Supseteq]{d} \\
    K \coloneqq k(x_1,\dotsc,x_n) \arrow[Supseteq]{r} & k[x_1,\dotsc,x_n]
  \end{tikzcd}
\end{equation*}
Let $f_0 := \operatorname{Norm}_{k(X)/K}(f)$.

Claim 1: $f_0 \in \pol{k}{x}{1}{n}$.

Claim 2: $\sqrt{(f_0)} = \pid \cap \pol{k}{x}{1}{n}$.

\textit{Proof of Claim 1:}
The polynomial $f$ is integrally dependent on $\pol{k}{x}{1}{n}$. Therefore there exists a monic polynomial $P$ with coefficients in $\pol{k}{x}{1}{n}$ such that $p (f) = 0$. Let $P(Y) = Y^n + \sum_{i=1}^n a_i Y^{n-1}$. The roots of $p(Y)$ are the conjugates $f_i$ of $f \in k(X)/K$.

Hence $P (f_i) = 0$ implies that $f_i$ is also integrally dependent on $\pol{k}{x}{1}{n}$, which in turn implies that the $a_i$ are integrally dependent on $\pol{k}{x}{1}{n}$.
But $a_i \in K$ and $\pol{k}{x}{1}{n}$ is integrally closed in $K$, therefore $a_i \in \pol{k}{x}{1}{n}$.
We have $\operatorname{char}\operatorname{pol}(m_f) = (\operatorname{min}\operatorname{pol} m_f)^l$ for some $l \geq 1$, therefore $f_0 = a_n^l \in \pol{k}{x}{1}{n}$.
%We have that $p$ is the minimal polynomial of $f$, therefore $P \mid \tilde P$, say $\tilde P = P Q$.
%We then have $\tilde P (f_i) = P(f_i) Q(f_i) = 0$.

\textit{Proof of Claim 2:}
As $t^n + \sum_{i = 1}^{n-1} a_i t^{n-i} \in (f)$, with $ 0 = P(f) = f^n + \sum_{i = 1}^{n-1} a_i f^{n-i} \in (f) + a_n$ we get $a_n \in (f) \cap \pol{k}{x}{1}{n}$.
Therefore $f_0 = a_n^l \in \sqrt{(f)} \cap \pol{k}{x}{1}{n} = \pid \cap \pol{k}{x}{1}{n}$, which shows ``$\subseteq$''.

Conversely, let $g \in \sqrt{(f)} \cap \pol{k}{x}{1}{n} = \pid \cap \pol{k}{x}{1}{n}$.
Then there exists some $m \geq 1$ such that $g^m \in (f)$.
This implies that there exists some $h \in k[X]$ such that $g^m = hf$.
We have $g^{m[k(X): K]}=\operatorname{Norm}(g^m) = \operatorname{Norm}(h)\operatorname{Norm}(f)$ (multiplicity of norm) and therefore $g^{m[k(X)\cdot K]} = f_0 h_0$ therefore $g \in \sqrt{(f_0)}$ (as $\operatorname{Norm}(f) = f_0$, $\operatorname{Norm}(h) = h_0 \in \pol{k}{x}{1}{n}$.

\textbf{Step 4:} Affine case.
	
Let $X = \mathbb A^n$ and $Z = V(f)$ irreducible, $\pid = I(X) = \sqrt{(f)}$.
$\pol{k}{x}{1}{n}$ is a unique factorization domain and $Z$ is irreducible, therefore $f = ug^l$ for a unit $u$ and $g \in \pol{k}{x}{1}{n}$ irreducible.
We have \[\operatorname{tr}\deg k(Z) = \operatorname{tr}\deg \operatorname{Frac}(\pol{k}{x}{1}{n} / (g) ) = n-1 = \dim \mathbb A^n -1\]
which finishes the proof.
\end{proof}
\begin{corollary} \label{dimLem1}
  For a variety $X$ and $f_1, \dotsc, f_r \in \MO_X (X)$, every irreducible component of $V(f_1,\dotsc,f_r)$ has codimension $\leq r$ ($\operatorname{codim} (Z,X) = \dim X - \dim Z$).
\end{corollary}
\begin{proof}
  Let $Z \subseteq V(f_1,\dotsc,f_r)$ be an irreducible component.
  Let $Z^\prime $ be a component of $V(f_1,\dotsc,f_{r-1})$ that contains $Z$.
  By induction, $\operatorname{codim}(Z^\prime,X) \leq r-1$.
  We have $Z \subseteq Z^\prime \cap V(f_r) \subseteq V(f_1,\dotsc,f_r)$, therefore $Z$ is an irreducible component of $Z^\prime \cap V(f_r) = V(\res{f_r}{Z^\prime})$ ($Z \subseteq X \subseteq Y$ each closed subsets and $Z$ irreducible component of $Y$ $\implies$ $Z$ an irreducible component of $X$).
  If $\res{f_r}{Z^\prime} = 0$, then $Z^\prime = Z$, which implies $\operatorname{codim} Z = \operatorname{codim} Z^\prime \leq r-1$.
  If $\res{f_r}{Z^\prime} \neq 0$, then by \Cref{Krull} we get $\operatorname{codim} Z = \operatorname{codim}Z^\prime +1 \leq r-1 +1 = r$.
\end{proof}
\begin{proposition}
  If $X$ is affine, and $Z \subseteq X$ is an irreducible and closed subset of codimension $r$, then there exists $f_1,\dotsc,f_r \in k[X]$ such that $Z$ is a component of $V(f_1,\dotsc,f_r)$ and every component of $V(f_1,\dotsc,f_r)$ is of codimension $r$.
\end{proposition}
\begin{proof}
  Claim:  There exist  $f_1,\dotsc,f_r \in k[X]$ such that
  \begin{enumerate}
  \item $Z_i$ is an irreducible component of $V(f_1,\dotsc,f_i)$
  \item Every irreducible component of $V(f_1,\dotsc,f_i)$ is of codimension $i$.
  \end{enumerate}
  We show this claim via induction over $i$.
  
  For $i =1$, let $0 \neq f_1 \in I(Z_1)$.  Let $Z^\prime$ be an irreducible component of $V(f_1)$ that contains $Z$, that is we have $Z \subseteq Z^\prime \subseteq X$.
  $Z$ and $Z^\prime$ both have codimension $1$ by \Cref{Krull}.
  This implies $Z_1 = Z^\prime$ by last time. % (?).

  Induction step:
  Let $Z_i \subseteq Z_{i-1} = Y_1 ,\dotsc,Y_l$ be components of $V(f_1,\dotsc,f_{i-1})$.
  Since $Y_j \setminus Z_i \neq \emptyset$ for $j \geq 1$, we have that there exists $g_j \in I(Z_i)$ that does not vanish on $Y_j$ for $j =1,\dotsc,l$.
  Therefore $I(Z_i) \not\subseteq I(Y_j)$ for $j =1,\dotsc,l$.
  By prime avoidance, we have $I(Z_i) \not\subseteq \bigcup_j I(Y_j)$.
  Let $f_i \in I(Z_i) \setminus \bigcup_j I(Y_j)$ (Check that $f_i$ works, similar to $i = 1$).
\end{proof}
\begin{note}
	Given $Z \subseteq X$, we cannot always find $f_1,\dotsc, f_r \in k[X]$ such that $V(f_1,\dotsc,f_r) = Z$, even if $r=1$.
	
	E.g. $S = (y^2z = (x+z)x(x-z)) \subseteq \mathbb \A^3$, $L \subseteq S$ line through origin.
	If $L$ is chosen correctly, there does not exist $f \in k[S]$ such that $V(f) = L$, even set theoretically.
\end{note}
\begin{proposition}
  Let $X,Y \subseteq \mathbb A^n$ be of codimension $e$ and $f$ respectively.
  Then every component of $X \cap Y$ has codimension $\leq e+f$.
\end{proposition}
\begin{proof}
  We have $X \cap Y \cong (X \times Y) \cap \Delta_{k^n} = \left(\res{(x_i - y_i)}{X\times Y}, i=1,\dotsc,n\right)$ is of codimension $\leq n$ in $X \times Y$ by \Cref{dimLem1}.
  We have $n \geq \operatorname{codim} (X\cap Y, X\times Y) = \dim X + \dim Y - \dim (X\cap Y) = n-e + n-f -\dim X\cap Y$, which implies $\dim X\cap Y \geq n-e-f$.
\end{proof}
\begin{proposition}
  For $X,Y \subseteq \mathbb P^n$ projective varieties of codimension $e$ and $f$,
  \begin{enumerate}[label=(\roman*)]
  \item Every component of $X \cap Y$ has codimension $\leq e+f$
  \item If $e +f \leq n$ then $X\cap Y \neq \emptyset$.
  \end{enumerate}
\end{proposition}
\begin{proof}
  $X = V(\ai)$, $\ai$ homogeneous. Let $X^\ast = V_{k^{n+1}}(\ai) \subseteq k^{n+1}$. Consider the isomorphism
  \begin{align*}X^\ast \cap D(x_i) & \longrightarrow (X \cap U_i) \times \mathbb A^1 \setminus \{0\} \\
  (x_0,\dotsc,x_n) & \longmapsto ([x_0,\dotsc,x_n],x_i) \\
  (ta_0/a_i,\dotsc,ta_n/a_i) & \longmapsfrom ([a_0,\dotsc,a_n],t) \,. \end{align*}
  Therefore \[\dim X^\ast = \dim (X^\ast \cap D(x_i)) = \dim (X \times \mathbb A^1 \setminus \{0\}) = \dim X + \dim \mathbb A^1 = \dim X + 1 \,.\]
  Therefore $\operatorname{codim}(X,\mathbb P^n) = e = \operatorname{codim} (X^\ast,k^{n+1})$.
  Let $e + f \leq n$.
  Since $ 0 \in X^\ast \cap Y^\ast$, there exists an irreducible component $Z \subseteq X^\ast \cap Y^\ast$.
  By the proposition, $\operatorname{codim}(Z,k^{n+1}) \geq e+f$.
  So $\dim Z \geq n+1 - (e+f) \geq 1$, therefore there exists $0 \neq (a_0,\dotsc,a_n) \in Z$, therefore $[a_0,\dotsc,a_n] \in X \cap Y$.
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
