\babel@toc {english}{}
\contentsline {chapter}{\numberline {0}Preliminaries}{5}{chapter.0}
\contentsline {part}{I\hspace {1em}Algebraic Geometry I}{6}{part.1}
\contentsline {chapter}{\numberline {1}Affine and Projective Varieties}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Affine Varieties}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}Projective Varieties}{13}{section.1.2}
\contentsline {chapter}{\numberline {2}Sheaves}{16}{chapter.2}
\contentsline {section}{\numberline {2.1}Basic Constructions and Sheafification}{16}{section.2.1}
\contentsline {section}{\numberline {2.2}Structure Sheaf}{19}{section.2.2}
\contentsline {chapter}{\numberline {3}Prevarieties}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}The Category of Prevarieties}{20}{section.3.1}
\contentsline {section}{\numberline {3.2}Affine Varieties as Prevarietes}{21}{section.3.2}
\contentsline {section}{\numberline {3.3}Projective Varieties as Prevarieties}{23}{section.3.3}
\contentsline {section}{\numberline {3.4}Products}{29}{section.3.4}
\contentsline {chapter}{\numberline {4}Varieties}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Seperatedness}{35}{section.4.1}
\contentsline {section}{\numberline {4.2}Function Field}{37}{section.4.2}
\contentsline {section}{\numberline {4.3}Dimensions}{39}{section.4.3}
\contentsline {section}{\numberline {4.4}Fibers of Morphisms}{46}{section.4.4}
\contentsline {chapter}{\numberline {5}Affine schemes / $\Spec R$}{50}{chapter.5}
\contentsline {section}{\numberline {5.1}The Zariski Topology on $\Spec R$}{50}{section.5.1}
\contentsline {section}{\numberline {5.2}Structure Sheaf}{56}{section.5.2}
\contentsline {chapter}{\numberline {6}Schemes}{60}{chapter.6}
\contentsline {section}{\numberline {6.1}The Category of Schemes}{60}{section.6.1}
\contentsline {section}{\numberline {6.2}Fiber Products}{69}{section.6.2}
\contentsline {section}{\numberline {6.3}Schemes over Schemes}{72}{section.6.3}
\contentsline {chapter}{\numberline {7}Properties of Schemes and Morphisms}{75}{chapter.7}
\contentsline {section}{\numberline {7.1}Affine Local Properties}{75}{section.7.1}
\contentsline {section}{\numberline {7.2}Immersions}{77}{section.7.2}
\contentsline {section}{\numberline {7.3}Dimensions}{82}{section.7.3}
\contentsline {section}{\numberline {7.4}Reduced Induced Subscheme Structure}{83}{section.7.4}
\contentsline {section}{\numberline {7.5}Separated Morphisms}{84}{section.7.5}
\contentsline {section}{\numberline {7.6}Valuative Criterion for Separatedness}{87}{section.7.6}
\contentsline {section}{\numberline {7.7}Proper Morphisms}{93}{section.7.7}
\contentsline {chapter}{\numberline {8}Normal Proper Curves over a Field}{97}{chapter.8}
\contentsline {chapter}{\numberline {9}Projective Spectrum}{102}{chapter.9}
\contentsline {section}{\numberline {9.1}The Zariski Topology on $\Proj A$}{102}{section.9.1}
\contentsline {section}{\numberline {9.2}The Structure Sheaf on $\Proj A$}{105}{section.9.2}
\contentsline {chapter}{\numberline {10}Quasi-coherent $\mathcal O_X$-modules}{108}{chapter.10}
\contentsline {section}{\numberline {10.1}The $\sim $-Construction}{108}{section.10.1}
\contentsline {section}{\numberline {10.2}Construction on $\mathcal O_X$-modules}{114}{section.10.2}
\contentsline {section}{\numberline {10.3}Coherent Sheaves}{116}{section.10.3}
\contentsline {section}{\numberline {10.4}Quasi-coherent Sheaves on $\Proj A$}{117}{section.10.4}
\contentsline {section}{\numberline {10.5}Locally Free Sheaves and Vector Bundles}{124}{section.10.5}
\contentsline {section}{\numberline {10.6}Relative $\Spec $ Construction}{126}{section.10.6}
\contentsline {section}{\numberline {10.7}Locally Free Sheaves and Vector Bundles II}{128}{section.10.7}
\contentsline {chapter}{\numberline {11}Regularity and Smoothness}{132}{chapter.11}
\contentsline {section}{\numberline {11.1}Regularity}{132}{section.11.1}
\contentsline {section}{\numberline {11.2}K\IeC {\"a}hler Differentials}{136}{section.11.2}
\contentsline {section}{\numberline {11.3}Regular versus Smooth}{145}{section.11.3}
\contentsline {section}{\numberline {11.4}Blow-up}{147}{section.11.4}
\contentsline {section}{\numberline {11.5}Du Val Singularities}{150}{section.11.5}
\contentsline {part}{II\hspace {1em}Algebraic Geometry II}{152}{part.2}
\contentsline {chapter}{\numberline {12}Sheaf Cohomology}{153}{chapter.12}
\contentsline {section}{\numberline {12.1}Motivation}{153}{section.12.1}
\contentsline {section}{\numberline {12.2}Definition, Existence and Uniqueness}{155}{section.12.2}
\contentsline {section}{\numberline {12.3}Construction using injective Resolutions}{158}{section.12.3}
\contentsline {section}{\numberline {12.4}Serre Vanishing Theorem}{163}{section.12.4}
\contentsline {chapter}{\numberline {13}\v Cech Cohomology}{169}{chapter.13}
\contentsline {section}{\numberline {13.1}Definition and Examples}{169}{section.13.1}
\contentsline {section}{\numberline {13.2}\v Cech Cohomology vs. Sheaf Cohomology}{172}{section.13.2}
\contentsline {chapter}{\numberline {14}Spectral Sequence of a Double Complex}{181}{chapter.14}
\contentsline {section}{\numberline {14.1}Motivation and Definition}{181}{section.14.1}
\contentsline {section}{\numberline {14.2}Application in Sheaf and \v Cech Cohomology}{184}{section.14.2}
