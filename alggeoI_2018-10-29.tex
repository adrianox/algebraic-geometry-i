\begin{definition}
  Let $X$ be a set, basis of topology $\mathcal{B} = \set{U\subseteq X}$ is a set of subsets such that
  \begin{enumerate}
  \item
    $X = \bigcup_{U \in \mathcal{B}} U$
  \item
    For all $U,V \in \mathcal{B}$ there exist $\set{ W_i}_i \subseteq \mathcal B$ such that $U \cap V = \bigcup_i W_i$.
  \end{enumerate}
  Given $\mathcal B$ define a topology of all unions of subsets of $\mathcal B$.
\end{definition}
\begin{example}
  For an affine variety $X$, we have a basis $\mathcal B = \setdef{D(f)}{f \in k[X]}$.
\end{example}
\begin{definition}[sheaf with respect to a basis]
  A \textbf{sheaf $\mathcal F$ with respect to $\mathcal B$} is a set $\mathcal F (U)$ for every $U \in \mathcal B$ and restriction morphisms $\mathcal F (V) \to \mathcal F(U)$ for every $U \subseteq V$ that satisfy the gluing and uniqueness with respect to $\mathcal B$, that is for all $U \in \mathcal B$ and for all covers $U = \bigcup_i U_i$ with $U_i \in \mathcal B$ we have
  \begin{enumerate}
  \item
    If $s,t \in \mathcal F(U)$ with $\res{s}{U_i} = \res{t}{U_i}$ for all $i$, then we have $s=t$.
  \item
    Given $s_i \in \mathcal F (U_i)$ such that for all $i,j$ there exists a cover $W_{i,j,k} \in \mathcal B$ of $U_i \cap U_j$ such that $\res{s_i}{W_{i,j,k}} = \res{s_j}{W_{i,j,k}}$ then there exists an $s \in \mathcal F (U)$ such that $\res{s}{U_i} = s_i$ for all $i$.
  \end{enumerate}
\end{definition}
\begin{proposition} \label{ExIndSh}
  Given a sheaf $\mathcal F$ on a basis $\mathcal B$ there exists a unique sheaf $\mathcal F^\prime$ on $X$ such that $\mathcal F (U) = \mathcal F^\prime (U)$ for all $U \in \mathcal B$ (and compatible restriction maps).
\end{proposition}
\begin{proof}
  Let $W \subseteq X$ open.
  Then define
	\[
    \setdef{s \colon W \to \bigcup_{p \in W} \mathcal F_p}{\begin{matrix}
      s(p) \in \mathcal F_p \text{ for all } P \in W\\
      \text{for all } p \in W \text{ there exists } p \in V \in \mathcal B \text{ and } t \in \mathcal F \\
      \text{such that } t_Q = s(Q) \text{ for all } Q \in V
    \end{matrix}}\,.\]
  Check
  \begin{enumerate}
  \item
    $\mathcal F^\prime$ is a sheaf
  \item
    $U \in \mathcal B$, $\mathcal F^\prime (U) = \mathcal F (U)$ by gluing and uniqueness of $\mathcal F$.
  \item
    For uniqueness prove universal property for maps to sheaves on $X$. \qedhere
  \end{enumerate}
\end{proof}
\begin{example}
  Let $X=V(\ai)$ be an affine algebraic set with the basis \[\mathcal B = \setdef{D(f)}{f \in k[X]}\] of the topology.
  Let $\mathcal O_X$ be the structure sheaf, and $R = \mathcal O_X (X) = k[X]$.
  Let $X^\prime = V(\ai,x_{n+1}f-1) \subseteq k^{n+1}$. We saw $\left(D(f), \res{\mathcal O_X}{D(f)}\right) \cong (X^\prime,\mathcal O_{X^\prime})$ via 
  \begin{align*}
  D(f) &\longrightarrow X^\prime \\
  (x_1,\dotsc,x_n) &\longmapsto (x_1,\dotsc,x_n,1/f) \\ (x_1,\dotsc,x_n) &\longmapsfrom (x_1,\dotsc,x_n,x_{n+1})
  \end{align*}
  and can compute
  \begin{align*}
    \mathcal O_X (D(f)) &= \mathcal O_{X^\prime}(X^\prime) \\
                        &= k[x_1,\dotsc,x_{n+1}]/(\ai,x_{n+1}f -1) \\
                        &= k[X][x_{n+1}](x_{n+1}f -1) \\ & = k[X]_f \\
                          &= k[X][1/f] \; .
  \end{align*}
We now have an alternate construction for $\mathcal O_X$:
\begin{itemize}
\item Define $\mathcal O_X$ on $\mathcal B$ by $\mathcal O_X (D(f)) := R_f$ with $R=k[X]$.
\item Define $\mathcal O_X$ on $X$ by \Cref{ExIndSh}.
\end{itemize}
\end{example}
%Last lecture, we discussed that the category of prevarieties has a product (\Cref{EP}).
\begin{remark} Let $X,Y$ be prevarieties. 
  \begin{enumerate}
  \item
    If $U \subseteq X$ is open, then $U \times Y \subseteq X \times Y$ is open.
    (Cover $U$ by affine open $U_i$, cover $Y$ by open affine $V_j$, then $U_i \times V_j$ is an open cover of $U \times Y$, we have $U \times Y = \bigcup_{i,j} U_i \times V_j$)
  \item
    If $Z \subseteq X$ is closed, then $Z \times Y$ is closed in $X \times Y$.
  \end{enumerate}
\end{remark}
\begin{proposition}
  Let $X,Y$ be projective varieties.
  Then $X \times Y$ is a projective variety.

  (where $(X,\mathcal O_X)$ is a prevariety $\iff X \subseteq \PR_k^n$ is an irreducible projective algebraic set.  $\mathcal O_X$ is the usual structure sheaf)
\end{proposition}
\begin{proof}
  Let $X \subseteq \PR_k^m$ and $Y \subseteq \PR_k^n$ be irreducible and closed. Then,
  \begin{align*}
    X \times Y = (X \times \PR_k^n) \cap (\PR_k^m \times Y) \subseteq \PR_k^m \times \PR_k^n
  \end{align*}
  is closed and irreducible (check this, $U \subseteq X$, $V \subseteq Y$ open, check that $U\times V$ is dense in $X \times Y$).
  It therefore suffices to show $\PR_k^m \times \PR_k^n$ is projective variety.

  For this, consider the Segre map.
  \begin{align*}
    \varphi \colon && \PR_k^m \times \PR_k^n \longrightarrow& \PR_k^{(m+1)(n+1)-1} = \PR_k^N \\ &&
    ([x_0,\dotsc,x_m],[y_0,\dotsc,y_n]) \longmapsto & [x_0y_0,\dotsc, x_my_0, x_0y_1,\dotsc,x_my_n] \\ && =& [x_iy_j]_{\substack{0 \leq i \leq m\\0 \leq j \leq n}} = [t_{i,j}]_{i,j}
  \end{align*}
  We claim that $\varphi$ is an isomorphism onto its image, and the image is closed.
  \begin{itemize}
  \item
    $\varphi$ is well-defined.
  \item
    $\varphi$ is a morphism:
    $\PR_k^N$ is covered by $U_{t_{i,j}} = \set{t_{i,j} \neq 0}$.
    We have \[\mathcal O_{\PR_k^N}(U_{t_{i,j}}) = k\left[\frac{t_{k,l}}{t_{i,j}} \mid k,l \right] \xrightarrow{\varphi^\ast} \mathcal O_{\PR^m \times \PR^n}(U_{x_i} \times U_{y_j}) = k\left[\frac{x_0}{x_i},\dotsc,\frac{x_m}{x_i},\frac{y_0}{y_j},\dotsc,\frac{y_n}{x_j}\right]\,,\] where $\varphi^\ast\left(\frac{t_{k,l}}{t_{i,j}}\right) = \frac{y_k}{x_i}\frac{y_l}{y_j}$.
    So $\varphi^\ast$ is a morphism.
  \item
    $\varphi$ is injective (check)
  \item
    $\varphi$ is an isomorphism
  \end{itemize}
  Let $Z=\im \varphi$.
  We check $\res{\varphi}{U_{x_i} \times U_{y_j}} \colon U_{x_i} \times U_{y_j} \to U_{t_{i,j}} \cap Z$ is an isomorphism (then $\varphi$ is an isomorphism since $\varphi$ is injective).
  Let us consider $i=j=0$. The other cases work similar. 
	\[\res{\varphi}{U_{x_0}\times U_{y_0}} \colon U_{x_0}\times U_{y_0} \rightarrow U_{t_{0,0}} \cap Z\]
  Let $X_i = \frac{x_i}{x_0}$, $Y_i = \frac{y_i}{y_0}$, $T_{i,j} = \frac{t_{i,j}}{t_{0,0}}$.
  On $U_{0,0} \cap Z$ we have $T_{i,j} = X_i Y_j = T_{i,0} T_{0,j}$ and $T_{i,0} = X_iY_0  = X_i$, $T_{0,j} = Y_j$. Therefore we have $U_{0,0} \cap Z \subseteq U_{0,0}$ is closed and $k[U_{0,0} \cap Z] = k[T_{i,j}] / (T_{i,j} - T_{i,0}T_{0,j}) \cong k[T_{i,0},\dotsc,T_{m,0},T_{0,j}\dotsc T_{0,n}]$.
  Note that $\left(\res{\varphi}{U_{x_0} \times U_{y_0}}\right)^\ast (T_{i,0}) = X_i$, $\left(\res{\varphi}{U_{x_0} \times U_{y_0}}\right)^\ast (T_{0,j}) = Y_j$ which implies that this is isomorphic to $k[X_i,Y_j]$ which again implies that $\left(\res{\varphi}{U_{x_0} \times U_{y_0}}\right)^\ast$ is an isomorphism.
  Since $\left(\res{\varphi}{U_{x_0} \times U_{y_0}}\right)^\ast$ is an iso, $\res{\varphi}{U_{x_0} \times U_{y_0}}$ is an iso.
\end{proof}

\chapter{Varieties}
\section{Seperatedness}
\begin{definition}[variety, seperatedness] \label{defVar}
  A prevariety $X$ is a \textbf{variety} if it is \textbf{separated}, that is for all prevarieties $Y$ and for all morphisms $g \colon Y \to X$ and $h\colon Y \to X$ the set \[ \setdef{y \in Y}{ g(y) = h(y)} \] is closed.
\end{definition}
\begin{example}~ \vspace{-\topsep}
  \begin{enumerate}
  \item
    Let $Y = \mathbb{A}^1$, $g,h \colon \mathbb{A}^1 \to X$ such that $\res{g}{\mathbb{A}^1 \setminus \{0\}} = \res{h}{\mathbb{A}^1 \setminus \{0\}}$.
    Then
	\[\set{g = h}
      = \begin{cases}\mathbb{A}^1 \setminus \{0\} & \text{if } g(0) \neq h(0), \text{ not closed}\\\mathbb{A}^1 & \text{if } g(0) = h(0), \text{ closed}\end{cases} \,.\]
    So if $X$ is a variety, then $g(0) = h(0) \implies$ ``the limit (if it exists) is unique''.
    More generally, $g \colon Y \setminus \{\operatorname{pt}\} \to X$ and $X$ is a variety implies that there exists at most one way to extend $g$ to $Y$.
  \item
    Let $X$ is the line with two origins which is constructed as follows
    $X_1 = \mathbb{A}^1_k$, $U_1 = X_1 \setminus \{0\}$, $X_2 = \mathbb{A}^1 \setminus\{0\}$, $U_2 = X_2 \setminus \{0\}$.
    Then we define $\varphi \colon U_1 \xrightarrow{\cong} U_2$, $x \mapsto y$ and 
    $X = (X_1 \cup X_2) / (p \in U_1 \sim \varphi(p) \in U_2)$.
    The basis of open sets in $X$ is $\mathbb{B} = \{\text{opens in }X_1\} \cup \{\text{opens in }X_2\} $, define the structure sheaf on the basis.
    $X$ is not a variety.
    \begin{figure}[h]
    	\centering
    	\begin{tikzpicture}[scale=3.0]
    		\draw[-),thick] (-1,0) -- (-0.01,0);
    		\draw[-),thick] (1,0) -- (0.01,0);
    		\fill[thick] (0,0.2) circle (0.02);
    		\fill[thick] (0,-0.2) circle (0.02);
    	\end{tikzpicture}
    	\caption{line with two origins}
    \end{figure}
  \item
    Claim:  If $X$ is an affine variety, it is a variety.

    Let $X \subseteq \mathbb{A}^1_k$ be an irreducible affine algebraic set, and let $g,h \colon Y \to X$.
    Then
    \[
      \setdef{y \in Y}{g(y) = h(y)} = \bigcap_{s \in k[X]} \left( g^\ast(s) - h^\ast(s)\right)^{-1} (0)\]
    is closed since $g(y) = h(y)$ iff $g^\ast(s)(y) - h^\ast(s)(y) = 0$, and $g^\ast(s) = h^\ast(s)$ is regular.

    Alternatively, we see $\set{g = h} = (\tilde{g} - \tilde{h})^{-1}(0)$, where $\tilde{g},\tilde{h} \colon Y \to X \to k^n$.
  \item
    If $X_1$, $X_2$ are varieties, then $X_1 \times X_2$ is a variety.
    $g = (g_1,g_2), h = (h_1,h_2) \colon Y \to X_1 \times X_2$, $\set{g = h} = \set{g_1 = h_1} \cap \set{g_2 = h_2}$.
  \item
    A projective variety $X$ is a variety.
    \begin{proof}
      $X \subseteq \PR_k^n$.
      If $n = 0$, then we are done.
      Otherwise, let $x,y \in \PR_k^n$.
      Then there exists a linear polynomial $h$ such that $x,y \in D(h)$ and $D(h)$ is affine.
      Then the claim follows by \Cref{lemPS}.
    \end{proof}
  \item
    Let $p_1,p_2 \colon Y = X \times X \to X$ where $p_i$ is projection to $i$-th factor.
    Then $\set{p_1 = p_2} = \setdef{(x,x)}{x \in X} = \Delta_X \subseteq X \times X$ (the diagonal).
    
    If $X$ is a variety, then $\Delta_X$ is closed.
  \end{enumerate}
\end{example}
\begin{proposition} \label{lemPS}
	If $X$ is a prevariety such that for all $x,y \in X$ there exists an open affine $U \subseteq X$ with $x,y \in U$, 
	then $X$ is a variety.
\end{proposition}
\begin{proof}
	Let $g,h \colon Y \to X$, $Z=\set{g=h}$.
	let $z \in \bar{Z}$ and let $x = g(z)$, $y = h(z)$.
	Pick $U \ni x,y$ affine.
	Now $z \in W = g^{-1}(U) \cap h^{-1} (U) \xrightarrow{\res{g}W,\res{h}W} U$.
	By the previous example we know $\set{\res{g}W = \res{h}W} = \set{g = h} \cap W$ is closed.
	Now $z \in \overline{\set{g = h}} \cap W = \overline{\set{\res{g}W = \res{h}W}} = \set{\res{g}W = \res{h}W}$.
	Therefore $z \in Z$, therefore we showed $\bar{Z} = Z$.
\end{proof}
\begin{proposition} \label{defVar2}
  Let $X$ be a prevariety.
  Then $X$ is a variety if and only if $\Delta_X \subseteq X\times X$ is closed
\end{proposition}
\begin{proof}
  Let $g,h \colon Y \to X$.
  Let $t = g \times h \colon Y \to X\times X$, and $\set{g = h} = t^{-1} (\Delta_X)$.
  Then $\Delta_X$ closed implies that $\set{g=h}$ is closed.
\end{proof}
\begin{note}
  A topological space $X$ is Hausdorff if and only if $\Delta_X \subseteq X\times X$ is closed.
  
  This does not imply Hausdorff $\iff$ seperate. For Hausdorff, we have the product topology, and before we had the product of prevarieties.
\end{note}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
