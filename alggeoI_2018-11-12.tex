\begin{remark}
  Let $X$ be an affine variety.
  Then $\dim X = n \iff \exists$ finite surjective morphism $X \to \mathbb{A}^n$.
\end{remark}
\begin{proof}
  By \Cref{NN}, there exists a finite surjection $f \colon X \to \A^n$.
  We need to show $n = \dim X$.
  \[\begin{tikzcd}
    k[X] \arrow[r] & k[X] \otimes_{k[X]} K  \\
    \pol{k}{x}{1}{n} \arrow[r] \arrow[u] & k(x_1,\dotsc,x_n) = K \arrow[u]
  \end{tikzcd}\]
  ($K$ is the field where we localized the functions pulled back from $\A^n$).
  
  Since $k[X] \otimes_{\pol{k}{x}{1}{n}} K$ is an integral domain and a finite $K$-module (integrally dependant), $k[X] \otimes_{\pol{k}{x}{1}{n}} K = k(X)$ is a field.
  Since $k(X)$ is integrally dependent over $K$, it is an algebraic extension.
  We have $\operatorname{tr}\deg k(X) = \operatorname{tr}\deg K = n$, which concludes what was to show for the remark.
\end{proof}
\section{Fibers of Morphisms}
For $f \colon X \to Y$, how do the dimensions of the fibers $f^{-1}(y)$ vary with $y$?
\begin{example}~ 
  \begin{enumerate}
  \item
    $U \subseteq Y$ open subset, $f \colon U \to Y$ inclusion.
    Then
    \begin{equation*}
      f^{-1}(y) = \begin{cases} \mathrm{pt}, & \text{if }y \in U \\ \emptyset, & \text{if } y \notin U\end{cases}
    \end{equation*}   
  \item  
    $f \colon \A^2 \to \A^2$, $(x,y) \mapsto (x,xy)$.
    Then $f^{-1} (a,b) = \setdef{(x,y)}{x =a, xy = b}$.
    
    Case $a \neq 0$.
    $x = a$, $xy = b$ implies $y = b/a$.

    Case $a= 0$.
    $x = 0$, which implies $b = 0$ and $y$ arbitrary.
	\[f^{-1}(a,b) = \begin{cases}\{(a,b/a)\}, & \text{if } a \neq 0\\ \{0\} \times k, &\text{if } a=0,b=0 \\ \emptyset, & \text{if } a=0,b\neq 0\end{cases}\]
  \end{enumerate}
\end{example}
Let $f \colon X \to Y$ be a morphism.
Assume that $f(X)$ is dense in $Y$.
(If not, replace $Y$ by $\overline{f(X)}$ with the induced variety structure from $Y$).
\begin{remark}
  For a variety $X$, $Z \subseteq X$ is a closed irreducible subset.
  We define $\MO_Z$ by
  \begin{equation*}
    \MO_Z (U) = \setdef{f \colon U \to k}{\exists \tilde U \subseteq X \text{ open s.t. } \tilde U \cap Z = U \text{ and } \exists \tilde f \in \MO_X (\tilde U) \text{ s.t. } \res{\tilde f}{U} = f}
  \end{equation*}
  $(Z, \MO_Z)$ is a variety.
\end{remark}
\begin{proposition} \label{fd>=r}
  Let $r = \dim X - \dim Y$, $y \in Y$.
  Every irreducible component of $f^{-1} (y)$ has dimension $\geq r$.
\end{proposition}
\begin{proof}
  We can assume that $Y$ is affine.
  Let $\dim Y = n$.
  Last proposition gives us $g_1, \dotsc, g_n \in k[Y]$ such that $V(g_1,\dotsc,g_n) = \set{y,y_2,\dotsc,y_l}$.
  Let $\tilde g_i = f^\ast (g_i)$.
  We have $V(\tilde g_1,\dotsc,\tilde g_n) = f^{-1} (y) \cup f^{-1} (y_2) \cup \dotsb \cup f^{-1} (y_n)$ (``$\supseteq$'' as we have irreducibility).
  By \Cref{dimLem1}, every irreducible component $Z \subseteq V(\tilde g_1,\dotsc,\tilde g_n)$ has $\operatorname{codim} (Z,X) \leq n = \dim Y$.
  Therefore $\dim X - \dim Z \leq \dim Y$.
\end{proof}
\begin{theorem} \label{fibdim}
  Let $f \colon X \to Y$ be dominant, $r = \dim X - \dim Y$.
  Then there exists a nonempty open $U \subseteq Y$ such that
  \begin{enumerate}[label=(\roman*)]
  \item $U \subseteq f(X)$
  \item For all $y \in U$ and for all irreducible components $Z$ of $f^{-1}(y)$, we have $\dim Z = r$
  \end{enumerate}
\end{theorem}
\begin{proof}
  Assume $Y$ is affine.
  We can also assume that $X$ is affine.
  (If $X$ is not affine, let $X = \bigcup_{i=1}^m X_i$ affine cover.
  Assume the theorem holds for $\res{f}{X_i} \colon X_i \to Y$.
  Pick $U_i$ as in the theorem, let $U = \bigcap_{i=1}^m U_i$)
  
  Idea: Use Noether normalization relative to $Y$.

  Claim:
  There exists a nonempty open $U \subseteq Y$ such that $\res{f}{f^{-1}(U)} = p_1 \circ \pi$, where $f^{-1}(U) \xrightarrow{\pi}U\times\A^r \xrightarrow{p_1} U$ (Note: $\pi$ is finite and surjective).

  Claim $\implies$ Theorem
  \begin{enumerate}
  \item $\pi$ is surjective
  \item For $y \in U$, $f^{-1}(y) = \pi^{-1}(y \times \A^r) \supseteq Z$ (irreducible component).
    Then $\dim Z = \dim \pi (Z) \leq \dim (y \times \A^r) = r$ (as $\pi$ is finite), ``$\geq$'' by \Cref{fd>=r}.
    
  \end{enumerate}
  \textit{Proof of the claim.}
  \begin{equation*}
    \begin{tikzcd}
      k[X] \arrow[r, hookrightarrow, "\text{integral}"] &  k[X] \otimes_{k[Y]} K \\
      & K[Y_1,\dotsc,Y_n] \arrow[u, "\text{int. dependent}"]  \\
      k[Y] \arrow[uu, "f^\ast"] \arrow[r, hookrightarrow]&  k(Y) = K \arrow[u, hookrightarrow]
    \end{tikzcd}
  \end{equation*}
  Apply \Cref{NN} to $k[X]\otimes_{k[Y]} K$ (relative to $K$).
  There exist $Y_1,\dotsc,Y_n \in k[X]\otimes_{k[Y]} K$ such that $Y_i = \sum_{j} g_{i,j} \otimes h_{i,j} = g_i / h_i$ for some $g_i \in k[X]$, $h_i \in k[Y]$.
  By replacing $Y$ by $D(h_1,\dotsc,h_n) = \bigcap_i D(h_i)$, we can assume $h_i = 1$.
  So $Y_i \in k[X]$.

  Let $t \in K[X]$.
  There exists $a_i \in \pol{K}{Y}{1}{n}$ such that $t^m + a_1t^{m-1} + \dotsb + a_m = 0$.
  let $b$ be the product of the denominators of the coefficients of $a_i$ ($a_i = \sum_{i,I} a_{i,I} / d_{i,I} Y^I$, $b = \prod_{i,I} d_{i,I}$).
  We have $b \in k[Y]$.
  Since $a_i \in k[Y]_b[Y_1,\dotsc,Y_n]$, so $t$ is integrally dependent on $k[Y]_b[Y_1,\dotsc,Y_n]$.
  Let $t_1, \dotsc, t_s \in k[X]$ be the generators over $k[Y]$.
  Let $b_1,\dotsc,b_s$ be as above.
  Then $t_1,\dotsc,t_s$ are integrally dependent on $k[Y]_{b_1 \cdot \dotsb \cdot b_s}[Y_1,\dotsc,Y_n]$ and $k[X]_b$ is integrally dependent on $k[Y]_{b_1 \cdot \dotsb \cdot b_s}[Y_1,\dotsc,Y_n]$.
  Set $U = D(b_1 \cdot \dotsb \cdot b_s)$.
  Then
  \begin{equation*}
    \begin{tikzcd}
      k[X]_{b_1 \cdot \dotsb \cdot b_s} & f^{-1} (U) \arrow[d, "\text{finite}"]  \\
      k[Y]_{b_1 \cdot \dotsb \cdot b_s}[Y_1,\dotsc,Y_r] \arrow[u, hookrightarrow, "\text{int. dependent}"] & u \times \A^r \arrow[d] \\
      k[Y]_{b_1 \cdot \dotsb \cdot b_s} \arrow[u]& U = D(b_1 \cdot \dotsb \cdot b_s)
    \end{tikzcd}
    \begin{tikzcd}
      X \arrow[d, "\text{dominating}"]& k[X]  \\
      Y & k[Y] \arrow[u, hookrightarrow]
    \end{tikzcd} 
  \end{equation*} 
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
