\chapter{Schemes}
\section{The Category of Schemes}
We want to define schemes as ``spaces'' which locally look like $(\Spec R, \MO_{\Spec R})$. 
Moreover, we want to define morphisms of schemes that locally are equivalent to ring homomorphisms $R \to S$.

\textbf{Observation.} A map of underlying topological spaces does not determine the pullback of functions. 

\begin{example}
	Let $R=k[x_1,x_2]/(x_1^2,x_1x_2,x_2^2)$ and $S=k[x]/x^2$. Then \[\Spec R=\{ (x^2,x_1x_2,x_2^2) \}=\{ * \} \] and $\Spec S = \{ (x)\}$. 
	Therefore, there is a unique map $\Spec S \to \Spec R$ but many different ring homomorphisms $R \to S, x_1 \mapsto ax , x_2 \mapsto bx$. 
	
	The pullback of functions is \textbf{additional data} in a specifying morphism. 
\end{example}
\begin{definition}[ringed space]
	A \textbf{ringed space} is a pair $(X,\MO_X)$ where 
	\begin{itemize}
		\item $X$ is a topological space.
		\item $\MO_X$ is a sheaf of rings on $X$.
	\end{itemize}
	A morphism of ringed spaces is $(f,f^\#) \colon (X, \mathcal O_X) \to (Y, \mathcal O_Y)$ where
	\begin{itemize}
		\item $f \colon X \to Y$ is continuous
		\item $f^\# \colon \MO_Y \to f_* \mathcal O_X$ is a morphism of sheaves of rings. 
	\end{itemize}
\end{definition}
\begin{definition}[scheme]
	A \textbf{scheme} is a ringed space $(X, \mathcal O_X)$ such that there exists an open cover $X=\bigcup_{i \in I} U_i$ such that \[(U_i, \res{\MO_X}{U_i}) \cong (\Spec R_i, \MO_{\Spec R_i})\] as ringed spaces for some ring $R_i$.
	
	A scheme $(X, \MO_X)$ is \textbf{affine} if $(X,\MO_X) \cong (\Spec R,\MO_{\Spec R})$.
\end{definition}
\textbf{Warning:} Schemes are called preschemes in Mumford. 
Schemes with some nice properties are called schemes in Mumford. 

Can we define a morphism of schemes to be a morphism of ringed spaces (consistent with our goals)?

Let $\varphi \colon R \to S$ be a ring homomorphism.
We define a morphism of ringed spaces \[(f_\varphi , f_\varphi^\#) \colon (\Spec S , \mathcal O_{\Spec S}) \to (\Spec R, \MO_{\Spec R}) \, .\]
We let 
\begin{align*}
	f_\varphi = \varphi^\ast \colon \Spec S \longrightarrow & \Spec R \\
	\pid \longmapsto & \varphi^{-1}(\pid) 
\end{align*}
which is continuous. 
	
We need to define \[f_\phi^\# \colon \MO_{\Spec R}(U) \to \MO_{\Spec S}(f^{-1}(U))\] for all $U \subset \Spec R$ (compatible with restrictions).
	
If $U=D(g)$, then $f_\varphi^{-1}(D(g))=D(\varphi(g))$. We define 
\[\begin{tikzcd}
	\MO_{\Spec R}(D(g)) \arrow[equal]{r} & R_g \arrow{d}[swap]{\varphi_g \eqqcolon f^\#(D(g))} & \frac{a}{g^n} \arrow[mapsto]{d} \\
	\MO_{\Spec S}(D(\varphi(g))) \arrow[equal]{r} & S_{\varphi(g)} & \frac{\varphi(a)}{\varphi(g)^n}
\end{tikzcd}\]
In general, let \[s \in \MO_{\Spec R}(U) = \setdef{s \colon U \to \bigcup_{\pid \in U} R_\pid}{\begin{matrix}
\exists U = \bigcup D(g_i) \text{ and } t_i \in R_{g_i} \\ \text{s.t. } s(\pid)=(t_i)_\pid \in R_{g_i} 
\end{matrix}}\]
We need to define \[\MO_{\Spec S}(f^{-1}(U)) \ni f_\varphi^\#(U)(s) \colon f_\varphi^{-1}(U) \to \bigcup\limits_{\qi\in f^{-1}(U)} S_\qi\] such that similar conditions as above hold.	
	
If $U = \bigcup_{\qi \in D(\varphi(g_i))} D(g_i) \implies f_\varphi^{-1}(U)=\bigcup D(\varphi(g_i))$.
If $\qi \in D(\varphi(g_i))$, then \[(f_\varphi^\#(U)(s))(\qi) = (\varphi_{g_i}(t_i))_\qi \in S_\qi \,.\] 
	
This is well-defined. If $\qi=f_\varphi(\pid)$ consider 
\[\begin{tikzcd}
	R_\pid \arrow{r} \arrow[bend right]{rr}{\varphi_\qi} & (R \setminus \pid)^{-1}S \arrow{r} & S_\qi
\end{tikzcd}\]
Then 
\[\begin{tikzcd}
	R_{g_i} \arrow{r} \arrow{d}{\varphi_{g_i}} & R_\pid \arrow{d}{\varphi_\pid} \\
	S_{\varphi(g_i)} \arrow{r} & S_\qi
\end{tikzcd}\]
does commute. 
	
Here $(\varphi_{g_i}(t_i))_\qi=\varphi_\pid((t_i)_\pid)=\varphi_\pid(s(\pid))$ is independent of $i$.
\begin{example}
	Not every morphism of ringed spaces $(\Spec S,\MO_{\Spec S}) \to (\Spec R, \MO_{\Spec R})$ is of this form.
	
	Let $S=k(x)$ with $\Spec S = \{ \bullet \}$ and $R=k[x]_{(x)}$ with $k=\overline k$ (Picture: \Cref{picC[x]_{(x)}}) % ?
	
	Let
	\begin{align*}
		f \colon \Spec k(x) & \longrightarrow \Spec R \\
		(0) & \longmapsto (x)
	\end{align*}
	and 
	\[f^\#(\{(0)\}) \colon \MO_{\Spec R}(\{(0)\}) \to \MO_{\Spec S}(\emptyset) = 0\] and 
	\[f^\#(\Spec R) \colon \MO_{\Spec R}(\Spec R) = R \to k(x) \] which is defined by the localization $k[x]_{(x)} \to k(x)$. 
	
	This is not induced by ring homomorphism $\varphi \colon R \to S$. If it were, then $f((0))=\varphi^{-1}(0)=(x)$ implies that $f_\varphi^\#(\Spec R)=\varphi$ is not injective since $(x) \in \ker$. 
	
 	Morphisms of ringed spaces is ``too weak'' of a notion. 
\end{example}
\begin{definition}[locally ringed space]
	A ringed space is a \textbf{locally ringed space} if for all $\pid \in X$, $\MO_{X,\pid}$ is a local ring. 
\end{definition}
\begin{example}
	$(\Spec R, \mathcal O_{\Spec R})$ is a locally ringed space. We have that $\MO_{\Spec R,\pid}=R_\pid \supset \pid R_\pid$ is local.
\end{example}
\begin{example}
	Every scheme is a locally ringed space. 
\end{example}
\begin{definition}[morphism of locally ringed spaces]
	A \textbf{morphism of locally ringed spaces} $(f,f^\#)$ is a morphism of ringed spaces such that for all $\pid \in X$ the induced map \[f^\#_\pid \colon \MO_{Y,f(\pid)} \to \MO_{X,\pid}\] is a local ring homomorphism ($(f^\#_\pid)^{-1}(\mi_\pid)=\mi_{f(\pid)}$). 
	
	where $f^\# \colon \MO_Y \to f_*\mathcal O_X$ induces the map 
	\[f_\pid^\# \colon \mathcal O_{Y,f(\pid)} \to \varinjlim_{\pid \in U} \MO_{X}(f^{-1}(U)) \to \MO_{X,\pid} \,.\]
\end{definition}
\begin{lemma}
	Let $\varphi \colon R \to S$ be a ring homomorphism, then $(f_\varphi,f_\varphi^\#)$ is a morphism of locally ringed spaces. 
\end{lemma}
\begin{proof}
	We have $f^\#_\pid=\varphi_\pid \colon R_\pid \to S_\qi$ where $f(\pid)=\qi$ and $\varphi^{-1}(\pid)=\qi$. We have 
	\begin{align*}
		f_\varphi \colon \Spec S \longrightarrow & \Spec R \\
		\pid \longmapsto & f(\pid)=\qi
	\end{align*}
	Then $\varphi_\pid^{-1}(\mi_\qi)=\varphi_\pid^{-1}(\qi \cdot S_\qi)=\varphi^{-1}_\pid(\qi) \varphi_\pid^{-1}(S_\qi)=\pid R_\pid = \mi_\pid$.
\end{proof}	
\begin{definition}[morphism of schemes]
	A \textbf{morphism of schemes} is a morphism of locally ringed spaces.
\end{definition}
\begin{definition}[value]
	
	Let $(X,\MO_X)$ be a locally ringed space. Let $U \subset X, g \in \MO_X(U)$ and $\pid \in U$. 
	
	The \textbf{value of $g$ at $\pid \in U$} is defined by \[g(\pid) = \text{Image of $g$ under } \MO_X(U) \xrightarrow{res} \MO_{X,\pid} \to \MO_{X,\pid}/{\mi_\pid}=\kappa(\pid)\]
	
	Furthermore, \[V(g) \coloneqq \setdef{\pid \in X}{g(\pid)=0} \, .\]
\end{definition}
This generalizes the notion for $\Spec R$.
\begin{lemma} \label{lemRinSp}
	Let $(X, \MO_X), (Y,\MO_Y)$ be locally ringed spaces. Let $(f,f^\#) \colon (X,\MO_X) \to (Y, \MO_Y)$ be a morphism of ringed spaces. 
	
	Then $(f,f^\#)$ is a morphism of locally ringed spaces $\iff$ For all $U \subset Y, g \in \MO_Y(U)$ we have $f^{-1}(V(g)) = V(f^\#(g))$.
\end{lemma}
\begin{proof}
	``$\impliedby$'': Consider $f_\varphi^\# \colon \MO_{Y,f(\pid)} \to \MO_{X,\pid}$. We know that $\left(f_\pid^\# \right)^{-1}(\mi_\pid) \subset \mi_{f(\pid)}$. 
	So, we need to show ``$\supset$''. 
	Equivalently. $f_\pid^\#(\mi_{f(\pid)})\subset \mi_\pid$.
	
	Let $g \in \mi_{f(\pid)}$. Let $U \subset Y$ be open and let $\tilde g \in \MO_Y(U)$ such that $\tilde g_\pid = g$. We have 
	\begin{align*}
		g \in \mi_{f(\pid)} \implies & f(\pid) \in V(\tilde g)  \\
		\implies & \pid \in f^{-1}(V(\tilde g)) \\
		\implies & \pid \in V(f^\#(\tilde g)) \\
		\implies & f^\#(\tilde g)_\pid = f_\pid^\#(g) \in \mi_p \\
		\implies & f^\#_\pid(\mi_{f(\pid)}) \subset\mi_\pid
	\end{align*}
	``$\implies$'': We have 
	\begin{align*}
		\pid \in f^{-1}(V(g)) \iff & f(\pid) \in V(g) \\
		\iff & g_{f(\pid)} \in \mi_{f(\pid)} = (f_\pid^\#)^{-1}(\mi_\pid) \\
		\iff & (f^\#(g))_\pid = f_\pid^\#(g_{f(\pid)}) \in \mi_\pid \\
		\iff & \pid \in V(f^\#(g))
	\end{align*}
	which finishes the proof.
\end{proof}
\begin{theorem} \label{HomSch}
	Let $X$ be a scheme, let $R$ be a ring. Then
	\begin{align*}
		\Theta \colon \Hom_{\Sch}(X, \Spec R) \longrightarrow & \Hom_{\Ring}(R,\MO_X(X)) \\
		(f,f^\#) \longmapsto & (f^\#(\Spec R) \colon R \to \MO_X(X))
	\end{align*}
	is a bijection. 
\end{theorem}
\begin{corollary}
	We have $\Hom_{\Sch}(\Spec S, \Spec R) = \Hom_{\operatorname{Rings}}(R,S)$
	
	In particular, every morphism of locally ringed spaces $\Spec S \to \Spec R$ is induced by a ring homomorphism. 
\end{corollary}
\begin{proof}
	Let $(f,f^\#) \colon \Spec S \to \Spec R$ be morphism of locally ringed spaces. Let $\phi = f^\#(\Spec R) \colon R \to S$ and let $(f_\varphi, f_\varphi^\#)$ be induced map by $\varphi$ (as in the example). 
	
	Then $f_\varphi^\#(\Spec R) = \varphi = f^\#(\Spec R)$. So by injectivity of \Cref{HomSch}, we have $(f,f^\#)=(f_\varphi,f_\varphi^\#)$.
\end{proof}
\begin{proof}[Proof of \Cref{HomSch}.] 
	
	\textbf{$\Theta$ is injective:} Given $(f,f^\#) \colon (X, \MO_X) \to (\Spec R, \MO_{\Spec R})$ We need to show that $(f,f^\#)$ is uniquely determined by $\varphi \coloneqq f^\#(\Spec R) \colon R \to \MO_X(X)$. 
	
	\textbf{Step 1:} $f$ uniquely determined by $\varphi$.
	
	Let $x \in X$ and $f(x) \in \Spec R$. For every prime $\pid\in \Spec R$ we have \[\pid = \setdef{a \in R}{a \in \pid}=\setdef{a \in R}{a(\pid)=0}\] Furthermore, \[a(f(x))=0 \iff f(x) \in V(a) \iff x \in V(f^\#(a)) \iff \varphi(a)(x) = 0\] by \Cref{lemRinSp}.
	Therefore, \[f(x)=\setdef{a \in R}{a(f(x))=0} = \setdef{a \in R}{\varphi(a)(x)=0}\] which is determined by $\varphi$. 
	
	\textbf{Step 2:} $f^\#$ is uniquely determined by $\varphi$.
	
	Consider $f^\# \colon \MO_{\Spec R}(U) \to \MO_X(f^{-1}(U))$. If $U=D(g)$, then
	\[\begin{tikzcd}
		R \arrow{r}{\varphi} \arrow{d} & \MO_X(X) \arrow{d} \\
		R_g \arrow{r}{f^\#(D(g))} & \MO_X(D(g))
	\end{tikzcd}\]
	Therefore, $f^\#(D(g))$ is uniquely determined by $\varphi$ by the universal property of the localization $R \to R_g$
	
	In general, if $U=\bigcup_{i \in I} D(g_i)$, then $\res{f^\#(U)}{f^{-1}(D(g_i))}$ is uniquely determined by $\varphi$. 
	Therefore, $f^\#(U)$ is uniquely determined by $\varphi$ (by sheaf property). 
	
	\textbf{$\Theta$ surjective:} 
	
	Let $\varphi \colon R \to \MO_X(X)$ be given. Let $X=\bigcup_{i \in I} U_i$ be a cover with $U_i \cong \Spec S_i$.
	Let \[\varphi \colon R \xrightarrow \varphi \MO_X(X) \to \MO_X(U_i)=S_i \,.\] Let $(f_i,f_i^\#) \colon U_i \to \Spec R$ be the induced morphisms by $\varphi_i$.
	
	\textbf{Claim:} On the overlaps $U_i \cap U_j$ we have $\res{(f_i,f_i^\#)}{U_i \cap U_j} = \res{(f_j,f_j^\#)}{U_i \cap U_j}$. 
	
	\textit{Proof of the claim.} Consider 
	\begin{align*}
		\res{f_i^\#}{U_i \cap U_j}(\Spec R) = & [\begin{tikzcd}[ampersand replacement = \&]
		R \arrow{r}{\varphi} \arrow[bend left]{rr}{\varphi_i} \& \MO_X(X) \arrow{r} \& \MO_X(U_i) \arrow{r} \& \MO_X(U_i \cap U_j)
		\end{tikzcd}] \\
		= & [\begin{tikzcd}[ampersand replacement = \&]
		R \arrow{r}{\varphi} \arrow[bend left]{rr}{\varphi_i} \& \MO_X(X) \arrow{r} \& \MO_X(U_j) \arrow{r} \& \MO_X(U_i \cap U_j)
		\end{tikzcd}] \\
		= & \res{f_j^\#}{U_i \cap U_j}(\Spec R)
	\end{align*}
	by considering 
	\[\begin{tikzcd}
	 & \MO_X(X) \arrow{dr} \arrow{dl} \\
	 \MO_X(U_i) \arrow{dr} & & \MO_{U_j} \arrow{dl} \\
	 & \MO_X(U_i \cap U_j)
	\end{tikzcd}\]
	By the injectivity of $\Theta$ we get the claim. 
	
	Now define $f \colon X \to \Spec R$ by $\res{f}{U_i}=f_i$. Given $V \subset \Spec R$ and $s \in \MO_{\Spec R}(U)$ define \[f^\#(V)(s)  \coloneqq \text{ unique element $t$ in } \MO_X(f^{-1}(U)) \text{ s.t.} \res{t}{f^{-1}(V) \cap U_i} = f_i^\#(V)(s) \] which satisfies the required properties.
\end{proof}
	\begin{corollary}
		There is a contravariant equivalence between
		\begin{itemize}
			\item Category of affine schemes
			\item Category of commutative rings with $1$. \qedhere
		\end{itemize}
	\end{corollary}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
