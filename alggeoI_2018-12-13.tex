\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
		\item[(c)] \textbf{Proof 1} (Assuming $X$ locally Noetherian) $X$ locally Noetherian $\implies$ $Y \to Y$ quasi-seperated. 
		We only need to check valuative criterion. Let \[\begin{tikzcd}
		U = \Spec K \arrow{r} \arrow{d} & X \arrow{d}{f} \\
		T = \Spec A \arrow[yshift=-2,xshift=2]{ur}[swap]{a} \arrow[yshift=2,xshift=-2]{ur}{b} \arrow{r} & Y \arrow{d} \\
		& Z  
		\end{tikzcd}\]
		By valuative criterion for $X \to Z$ we have $a=b$.
		
		\textbf{Proof 2} Let $Z=S$. Factor $f$ as follows \[\begin{tikzcd}
		X \arrow{r}[swap]{\Gamma_f=\id \times f} \arrow{d}{f} \arrow[bend left]{rr}{f} & X \times_S Y \arrow{d}{f \times \id} \arrow{dr}{p_1} \arrow{r}{p_2} & Y \arrow{dr} \\
		Y \arrow{r}{\Delta_Y}  & X \times_S Y & X \arrow{r}{g} & S
		\end{tikzcd}\]
		where both squares are pullbacks.
		
		We know that $\Delta_Y$ is a locally closed immersion. 
		Thereofore, $\Gamma_f$ is also a locally closed immersion and hence seperated (closed immersion are seperated and this property is local on target).
		Furthermore, $g \colon X \to S$ is seperated by assumption. Therefore, $p_2$ is also seperated. Therefore, $f=p_2 \circ \Gamma_f$ is seperated. \qedhere
	\end{enumerate}
\end{proof}
\section{Proper Morphisms}
We have \vspace{\topsep}

\begin{tabular}{|c|c|c|}
	\hline 
	\textbf{Property} & \textbf{Top. analog}  & \textbf{Interpretation} \\ 
	\hline 
	Seperatedness & Hausdroff & Uniqueness of limits \\ 
	Universally closed & Quasicompactness & Existence of limits \\
	Proper & Compactness & Uniqueness + Existence of limits \\
	\hline
\end{tabular} 
\begin{definition}[universally closed, proper] A morphism $f \colon X \to Y$ is \textbf{universally closed} if for all $Y^\prime \to Y$ the base change $f^\prime \colon X^\prime \to Y^\prime$ is closed, where \[\begin{tikzcd}
	X^\prime \arrow{r} \arrow{d}{f^\prime} & X \arrow{d}{f} \\
	Y^\prime \arrow{r} & Y \; .
	\end{tikzcd}\]
	
	A morphism $f \colon X \to Y$ is \textbf{proper} if $f$ is separated, of finite type and universally closed.
\end{definition}
\begin{example}
	The scheme $X = \A_k^1$ is not proper over $k$. Consider \[\begin{tikzcd}
	\A_k^2 \arrow{r} \arrow{d}{p} & X \arrow{d} \\ 
	\A_k^1 \arrow{r} & \Spec k
	\end{tikzcd}\]
	The map $p$ is not closed since $p(V(xy-1))=D(x)$.
\end{example}
\begin{example}[finite morphisms are proper]
	E.g. $\Spec \C[x,y]/(y^2-x^3+x) \to \Spec \C[x]$.
\end{example}
\begin{theorem}[Valulative criterion for properness]
	Let $f \colon X \to Y$ be a morphism. The following are equivalent 
	\begin{enumerate}
		\item $f$ is proper
		\item $f$ is quasi-compact and of finite type and for all valuation rings $A$ with $K=\Frac A$ and for all commutative diagrams \[\begin{tikzcd}
		U = \Spec K \arrow{r} \arrow{d} & X \arrow{d}{f} \\
		T = \Spec A \arrow{r} \arrow[dashed]{ur}{\exists !} & Y
		\end{tikzcd}\]
		there exists a unique map $T \to X$ such that the whole diagram commutes. 
	\end{enumerate}
\end{theorem}
\begin{proof}
	``$\implies$'': Let $f$ be proper. 
	Then, $f$ is quasi-seperated and of finite type.
	Let $p \colon X^\prime \to \Spec A$ be obtained from $f \colon X \to Y$ via base-change $\Spec A \to Y$. The morphism $p$ is closed since $f$ is universally closed. The morphism $\Spec K \colon X$ factors through $X^\prime$ by the universal property of the fiber product. \[\begin{tikzcd}
	\Spec K \arrow{r}{a} \arrow{dr} & X^\prime \arrow{d}{p \text{ closed}} \arrow{r} & X \arrow{d}{f}\\
	& \Spec A \arrow{r} & Y
	\end{tikzcd}\]
	Let $a \colon \Spec K \to X^\prime$.
	Let $\xi_1 = a((0)) \in X^\prime$.
	Let $Z=\overline{\{\xi_1 \}}$ with reduced induced subscheme structure.
	
	Then $p(Z)$ is closed in $\Spec A$ and $(0) \in p(Z)$. 
	Therefore, $p(Z) = \Spec A$.
	Let $\xi_0 \in Z$ such that $p(\xi_0)=\mi$ is the closed point in $\Spec A$.
	Consider \[\begin{tikzcd}
	A=A_\mi \arrow{r}{(\res{p}{Z})^\#_{\xi_0}}\arrow{d} & \MO_{Z,\xi_0} \arrow{d}{\operatorname{res}} \\
	K \arrow[hook]{r}{(\res{p}{Z})^\#_{\xi_1}} \arrow[bend right]{rr}{\id} & \MO_{Z,\xi_1} = \Frac \MO_{Z,\xi_1} \arrow[hook]{r}{a^\#} & K
	\end{tikzcd}\]
	Therefore, $\MO_{Z,\xi_1} = K$ and $\MO_{Z,\xi_0}$ dominates $A$.
	We get $\MO_{Z,\xi_0}=A$ since $A$ is valuation ring. Therefore, there exists an inverse $\psi \colon \MO_{Z,\xi_0} \to A$. 
	Let $g \colon \Spec A \to \Spec \MO_{Z,\xi_0} \to Z \to X$ be the morphism induced by $\psi$. 
		By construction, $g$ is a section of $p$. Therefore, $(X^\prime \to X) \circ g$ is the desired lift. 
		
	``$\impliedby$'' Let $f$ satisfy the valuative criterion in the theorem. 
	We show that $f$ is universally closed.
	For all $Y^\prime \to Y$ consider \[\begin{tikzcd}
		X^\prime \arrow{r} \arrow{d}{f^\prime} & X \arrow{d}{f} \\
		Y^\prime \arrow{r} & Y
		\end{tikzcd}\]
		\textbf{Step 1.} We check that $f^\prime$ satisfies the conditions. \[\begin{tikzcd}
		U = \Spec K \arrow{r} \arrow{d}{} & X^\prime \arrow{r} \arrow{d}[swap,yshift=4]{f^\prime} & X \arrow{d}{f} \\
		T = \Spec A \arrow{r} \arrow[dashed]{ur}{\exists !} \arrow[bend right=10]{urr}[swap,xshift=13,yshift=5]{\exists ! g} & Y^\prime \arrow{r} & Y
		\end{tikzcd}\]
		We know that there exists a unique lift $g \colon T \to X$. By the universal property of the fiber diagram there exists a unique $g \colon T \to X^\prime$.
		
		\textbf{Step 2.} We check that $f^\prime$ is closed. Let $Z \subset X^\prime$ be closed subset with reduced induced subscheme structure. 
		We show that $f(Z)$ is stable under specialization. 
		Let $x_0,x_1 \in Y^\prime$ such that $x_0 \in \overline{ \{x_1\}}$ and $x_1 \in f^\prime(Z)$. 
		Let $\xi_1 \in Z$ such that $f^\prime(\xi_1)=x_1$.
		Let $A^\prime = \MO_{\overline{\{x_1 \}},x_0} \subset \kappa(x_1) = \MO_{\overline{\{x_1 \}},x_1}$.
		Let $A$ be a valuation ring in $\kappa(\xi_1)=\MO_{\overline{\{\xi_1\}},\xi_1}$ that dominates $A^\prime$ via $(f^\prime)^\# \colon \kappa(x_1) \hookrightarrow \kappa(\xi_1)$. Consider \[\begin{tikzcd}
		K = \kappa(\xi_1) & \arrow[hook]{l} \kappa(x_1) \\
		A \arrow[hook]{u} & \MO_{\overline{\{x_1\}},x_0} \arrow{l}{\text{loc.}} \arrow{u}
		\end{tikzcd}\] which gives 
		\[\begin{tikzcd}
		\Spec K \arrow{r}{i_{\xi_1}} \arrow{d} & X^\prime \arrow{d}{f^\prime} \\
		\Spec A \arrow{ur}{\exists g} \arrow{r} & Y^\prime \\
		\mi \arrow[mapsto]{r} & x_0 \\
		(0) \arrow[mapsto]{r} & x_1
		\end{tikzcd}\]
		We get $g \colon \Spec A \to Z \to X^\prime$ and therefore $x_0=f^\prime(g(\mi)) \in f^\prime(Z)$.
	\end{proof}
	\begin{lemma}[discrete valuation ring]
	Let $A$ be a ring which is not a field. The following are equivalent. 
	\begin{enumerate}
		\item $A$ is Noetherian valuation ring.
		\item $A$ is local, $1$-dimensional, integrally closed, Noetherian domain.
		\item $A$ is local principal ideal domain.
		%>>>>>>> e980649bb36e7d3d192b9da471a24b5a62d0fa5c
		\item $A$ is integral domain, $K= \Frac A$ and $\exists v \colon K^* \to \Z$ such that $v(ab)=v(a)+v(b)$ and $v(a+b) \ge \min \{v(a),v(b)\}$ such that $A = \setdef{a \in K^*}{v(a) \ge 0} \cup \{0\}$.
	\end{enumerate}
	If these properties hold we say that a is a \textbf{discrete valuation ring}.
\end{lemma}
\begin{remark}
	If $f \colon X \to Y$, $Y$ locally Noetherian if suffices to check valuation criterions for discrete valuations rings instead of all valuation rings (harder commutative algebra).
\end{remark}
\chapter{Normal Proper Curves over a Field}
\begin{definition}[normal]
	A scheme $X$ is called \textbf{normal} if the stalk $\MO_{X,p}$ is integrally closed at every point $p \in X$.
\end{definition}
\begin{definition}[function field of dimension $1$]
	A \textbf{function field of dimension $1$ over $k$} is a finitely generated field extension $K/k$ of transcendence degree $1$.
\end{definition}
	Let $C$ normal proper curve over field $k$ $\iff$ variety of dimension $1$. 
	
	Let $\eta \in C$ be generic point. 
	Let $K=\kappa(\eta)=\MO_{C,\eta}$ be the function field of $C$.
	
	Let $x \in X$ be a closed point, Then $\MO_{C,x}$ is local, integrally closed, Noetherian and $\dim \MO_{C,x}=1$ (since $\dim C = 1$, $\dim \{x\}=0$ and $C$ is of finite type, so $\dim C - \dim \{X\} = \codim (\{x\},C)=\dim \MO_{C,x}$). 
	All in all, $\MO_{C,x}$ is a discrete valuation ring of $K/k$  where $K=\Frac A$, $k \subset A$.
	
	Conversly, let $A$ be a discrete valuation ring of $K/k$. Consider \[\begin{tikzcd}
	U = \Spec K \arrow{r}{i_\eta} \arrow{d} & C \arrow{d} \\
	T = \Spec A \arrow[hook]{ur}{\exists ! f_A} \arrow{r} & \Spec k
	\end{tikzcd}\]
	Let $x = f_A(\mi)$ with $(f_a^\#)_\mi \colon \MO_{C,x} \to A \to K$ which gives $\MO_{C,x} \subset A \subset K$ where $\MO_{C,x},A$ are discrete valuation ring which implies $\MO_{C,x} = A$. 
	
	Upshot: There is a correspondence 
	\begin{align*}
		\{\text{closed point in }C \} \longrightarrow & \{\text{DVR of } K/k \} \\
		x \longmapsto & \MO_{C,x}
	\end{align*}
	We have 
	\begin{align*}
		\{\text{normal proper curves}/k \} & \longrightarrow \{\text{functions fields of dimension }1/k \} \\
		C & \longmapsto K(C)
	\end{align*} 
	Is there an inverse map $\Psi$? 
	We can construct it as follows: As a set \[C = \{ \text{valuation rings of }K/k \}=\{\text{DVR of }K/k \} \sqcup \{ [K] \}\]
	For $p \in C$ we write $A_p$ for associated valuation ring. Define that $Z \subset C$ is closed if
	\begin{enumerate}
		\item $Z=C$ or
		\item $Z$ is finite subset of $C \setminus \{[K]\}$ %?
	\end{enumerate}
	Let $U \subset C$ be nonempty open. Define \[\MO_C(U) \coloneqq \bigcap_{p \in U} A_p \subset K \,.\]
	
	\textbf{Claim:} $(C,\MO_X)$ is a scheme. 
	
	We compute \[\MO_{C,p} = \varinjlim\limits_{p \in U} \bigcap_{q \in U} A_q = A_p\] which is a local ring. Therefore, $(C,\MO_C)$ is a locally ringed space.
	
	We need to check that there exists an open cover by affine schemes. 
	For all $0 \neq x\in K$, define $U_x = \{p \in C | x \in A_p\}$.
	We claim that $U_x$ is open and that $(U_x,\res{\MO_C}{U_x})$ is affine and that $\setdef{U_x}{x \in K}$ covers $C$.	
	
	$U_x$ is open $\iff$ $C \setminus U_x$ is finite.
	We have $p \in C \setminus U_x$ $\iff$ $x \notin A_p$ $\iff$ $1/x \in \mi_p \subset A_p$. 
	Let $y=1/x$. Let $R=$integral closure of $k[y] \subset K$. Then $K/k(y)$ is a finite field extension and therefore $R$ is a finitely generated $k$-algebra with $\dim R = \tr\deg R = \tr \deg K =1$.
	Therefore, $R_\qi$ is a discrete valuation ring for all prime ideals $0 \neq \qi \subset R$. 
	
	We claim that there is a bijection 
	\begin{align*}
		U_y & \longrightarrow \Spec R \\
		p & \longmapsto \qi = \mi_p \cap R
	\end{align*}
	Let $p \in C$ be closed such that $y \in A_p$. Then $k[y] \subset A_p$. Therefore, $R \subset A_p$ with $\qi = \mi_p \cap R$ we have $R_\qi \subset A_p$. Because DVR $\implies$ $R_\qi = A_p$.
	
	Now \[C \setminus U_x = \setdef{p \in U_y}{y \in \mi_p} \cong \setdef{\qi \in \Spec R}{y(\qi)=0}=V(y)\] which is a finite set of points since $\dim R = 1$ (by \Cref{Krull}).
	
	Let $R=$integral closure of $k[x]$ in $K$. We have $U_x \cong \Spec R$ since \[\MO_{\Spec R}(V) = \bigcap_{x \in V} \MO_{\Spec R,x} = \bigcap_{x \in V} A_x = \MO_C(V) \, .\]
	
	The rest is left for the reader as an easy exercise. 
\begin{example}
	Consider $K=\C(t)$ over $k=\C$. Let $A \subset K$ be a discrete valuation ring.
	
	\textbf{Case 1.} $t \in A$. Then $\C[t] \subset A \subset \C(t)$ which implies $A=\C[t]_{(t-a)}$ for some $a \in C$. 
	
	\textbf{Case 2.} $u=1/t \in A$. Then $A = \C[u]_{(u-b)}$ for some $b \in C$. But if $a \neq 0$, then $\C[t]_{(t-a)} = \C[u]_{u-1/a}$.
	
	Therefore, $C=(U_t \sqcup U_u /\sim \varphi) \cong \PR_\C^1$ where $U_t = \Spec \C[t]$ and $U_u = \Spec \C[u]$ and $\varphi \colon U_t \setminus \{0\} \to U_u \setminus \{0\}, t=1/u \mapsto u$. 
	%Let $K=\C(x,\sqrt{x^3-x})$ and set $y=\sqrt{x^3-x}$. Then $U_x = \Spec \C[x,y]/(y^2-x^3+x)$ 
\end{example}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
