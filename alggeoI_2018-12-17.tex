\begin{example}
  $K = \C(x,\sqrt{x^3 -x})$, $k = \C$.
  $C_K = \set{\text{discrete valuation rings of $K/k$}} \sqcup \set{K}$.
  Let $t \in K$.
  Then $U_t = \setdef{p \in C_K}{t \in A_p}$, where $A_p$ is the valuation ring corresponding to $p$.
  For every valuation ring $A \subseteq K$, we have $x \in A$ or $1/x \in A$.
  Therefore $U_x \cup U_{1/x} = C$.

  Here:  $R$ is the integral closure of $k[x]$ in $K$.
  Let $y = \sqrt{x^3-x}$.
  Then $y^2 = x^3 -x$, $R = k[x,y]/(y^2 - (x^3  -x))$, $U_x = \Spec R$.

  For $U_{1/x}$:  Let $t = 1/x$.
  Let $S$ be the integral closure of $k[t]$ in K.
  Let $u = \frac{y}{x^2} = \frac{\sqrt{x^3 - x}}{x^2}$, $u^2 = \frac{x^3 -x}{x^4} = \frac{1}{x} - \frac{1}{x^3} = t - t^3$.
  Therefore $S = k[t,u]/(u^2 - (t-t^3))$, $C = \Spec R \cup \Spec S$.
\end{example}
\begin{lemma}
  $C = C_K$ is a normal proper curve over $k$.
\end{lemma}
\begin{proof}
  $C$ is covered by $U_x$ and $U_{1/x}$.
  We have $k[t] \subseteq K$, $R$ is the integral closure of $k[t]$ in $K$, and since $K$ is finite over $k(t)$, $R$ is of finite type over $k$.
  Therefore $C$ is of finite type.

  Normality: $\MO_{C,p} = A_p$ is integrally closed by definition.
  
  Properness:  Use the valuation criterion.
\end{proof}
\begin{theorem} \label{CatEqCurves}
  There is a contravariant equivalence of categories between
  \begin{enumerate}
  \item
    the category of proper normal curves over $k$ with non-constant morphisms and
  \item
    function fields of dimension $1$.
  \end{enumerate}
  given by
  \begin{align*}
    \Phi \qquad \qquad \colon C & \longrightarrow K(C)  \\
    (f \colon C \to C^\prime) &\longmapsto (f^\#_\eta \colon K(C^\prime) \to K(C)) \; ,
  \end{align*}
  where $\eta,\eta^\prime$ denote the generic points of $C$ and $C^\prime$, respectively. This is well-defined since $f(\eta) = \eta^\prime$, as $f$ is non-constant.
\end{theorem}
\begin{lemma} \label{helplemmaCurves}
  Let $X,Y$ be schemes over $k$.
  Let $x \in X$, $y \in Y$, and let $Y$ be of finite type.
  Let $\varphi_c \colon \MO_{Y,y} \to \MO_{X,x}$.
  Then there exists an open neighborhood $U$ of $x$ and a morphism $f \colon U \to Y$ such that $f(x) = y$ and $(f^\#_x \colon \MO_{Y,y} \to \MO_{X,x}) = \varphi_x$.
\end{lemma}
\begin{proof}
  We can assume that $X$ and $Y$ are affine.
  We write $X = \Spec A$, $Y = \Spec B$, $B = \pol{k}{x}{1}{n}/(g_1,\dotsc,g_m)$, $x = \pid$, $y = \qi$.

  Let $t \in A$ such that there exist $y_1,\dotsc,y_n \in A_t$ such that $\varphi_x(x_i) = (y_i)_\pid$
  (e.g. let $t$ be the product of all denominators of the $y_i$).
  Let $\tilde t \in A$ such that $g_j(y_1,\dotsc,y_n) = 0$ in $A_{t\tilde t}$ for all $j$.
  Then
  \begin{equation*}
    \begin{tikzcd}
      \varphi_x \colon & B_\qi \arrow[r,"\varphi_x"] &A_\pid & \ni \varphi_x (x_i) \\
      & B \arrow[u]\arrow[r] & A_{t\tilde{t}} \arrow[u] & \\
      g_i \in & \pol{k}{x}{1}{n} \arrow[u]\arrow[r] & A_t \arrow[u] &
    \end{tikzcd}
  \end{equation*}
  gives a morphism $B \to A_{t\tilde t}$ which induces $\Spec A_{t \tilde t} \to \Spec B$ with the desired properties.
\end{proof}
\begin{proof}[Proof of \Cref{CatEqCurves}.]
  Construct a quasi-inverse $\Psi$:

  On objects: $\Psi \colon K \to C_K$

  On morphisms: For $\varphi \colon K^\prime \to K$ we define $\Psi(\varphi) \colon C \to C^\prime$, $C = C_K$, $C^\prime = C_{K^\prime}$.
  By applying \Cref{helplemmaCurves} to $\eta \in C$, $\eta^\prime \in C^\prime$ and $K^\prime \to K$, there exists a nonempty open $U \subseteq C$ and a morphism $f \colon U \to C^\prime$ such that $f_\eta = \varphi$.
  Therefore $U = C/\set{p_1,\dotsc,p_n}$.

  Using \Cref{extensionCurves}, set $\Psi (\varphi) \coloneqq \bar{f}$.
  Then $\Phi(\Psi(\varphi)) = \varphi$, and $\Psi(\Phi(f)) = f$ (easy to check using separatedness).
\end{proof}
\begin{lemma} \label{extensionCurves}
  Let $C$ be a normal curve over $k$, $p \in C$ a closed point.
  Let $X$ be a proper scheme over $k$. 
  Then every morphism $f \colon C \setminus \set{p} \to X$ extends uniquely to a morphism $C \to X$.
\end{lemma}
\begin{proof}
  $K = K(C)$.
  \begin{equation*}
    \begin{tikzcd}
      \Spec K \arrow[d]\arrow[r,hookrightarrow,"i_{\eta}"] & C \setminus\set{p} \arrow[d,hookrightarrow] \arrow[r,"f"] & X \\
      \Spec \MO_{C,p} \arrow[r] \arrow[urr,dashed,swap,"\exists g"] \arrow[dr] &C \arrow[r] &\Spec k  \\
      & V \arrow[u,hookrightarrow] \arrow[ruu,bend right=100,xshift=2,"h"]
    \end{tikzcd}
  \end{equation*}
  By properness of $X$, there exists a $g \colon \Spec \MO_{C,p} \to X$.
  Let $(g^{\#})_p \colon \MO_{X,q} \to \MO_{C,p}$, $q = g(p)$, $p = \mi_p \in \Spec \MO_{C,p}$.
  By \Cref{helplemmaCurves}, there exists an open $V \subseteq C$ and a morphism $h \colon V \to C$ such that $g^\#_p = h^\#_p$.
  By construction,
  \begin{align*}
    & \res{h}{\Spec K} = \res{g}{\Spec K} = \res{f}{\Spec K}  \\
    \implies & \res{h}{W} = \res{f}{W} \text{ for some non-empty open } W \subseteq V \setminus \set{p} \subseteq X  \\
    \implies & \res{h}{V \setminus \set{p}} = \res{f}{V\setminus \set{p}} \text{ by separatedness of } X 
  \end{align*}
  Let $\bar f \colon C \to X$ be a morphism obtained from gluing $h$ and $f$ along $V \setminus \set{p}$ ($\bar f$ unique since $X$ separated).
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
