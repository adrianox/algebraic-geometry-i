\section{Construction on $\MO_X$-modules}
\begin{definition}[tensor product]
	Let $\F,\G$ be $\MO_X$-modules. Let \[(\F \otimes_{\MO_X} \G) \coloneqq \text{sheaf associated to presheaf } U \mapsto \F(U) \otimes_{\MO_{X}(U)} \G(U)\] be the \textbf{tensor product of $\F$ and $\G$}.
\end{definition}
\begin{lemma}
	If $\F,\G$ are quasi-coherent, then so is $\F \otimes_{\MO_X} \G$ quasi-coherent and for all open affine $U$ we have $(\F  \otimes_{\MO_X} \G)(U) = \F(U) \otimes_{\MO_X(U)} \G(U)$. 
\end{lemma}
\begin{definition}[$\Hom$-sheaf, dual]
	Let $\F,\G$ be $\MO_X$-modules. Let \[\Hom_{\MO_X}(\F,\G)(U) \coloneqq \Hom_{\res{\G_X}U} (\res{\F}U, \res{\G}U)\] be the \textbf{$\Hom$-sheaf of $\F$ and $\G$}.
	
	The \textbf{dual} is defined to be \[\mathcal F^\vee \coloneqq \Hom_{\MO_X}(\F,\MO_X) \,.\]
\end{definition}
\begin{lemma} \label{pushforwardquasi}
	Let $f \colon X \to Y$ be a morphism of schemes and let $\F$ be an $\MO_X$-module and $\G$ be an $\MO_Y$-module. Then, the pushforward $f_\ast\F$ is an $\MO_Y$-module and the pullback $f^\ast \G$ is an $\MO_X$-module.
	
	E.g. if $\G$ is quasi-coherent, then so is $f^\ast \G$.
	
	If $\F$ is quasi-coherent and $f$ is quasi-seperated and quasi-compact, then $f_\ast \F$ is quasi-coherent.
\end{lemma}
\begin{definition}[locally free]
	An $\MO_X$-module is \textbf{locally free} is for all $x \in X$ there exists an open neighbourhood $U$ of $x$ and an index set $I$ such that \[\res{\F}{U} = \res{\MO_X}U^{\oplus I} \,.\]
	In this case, the \textbf{rank} is defined to be \[\operatorname{rk}_X(\F) \coloneqq \text{cardinality of }I\] which is a locally constant function of $X$.
\end{definition}
\begin{remark}
	If $\F$ is locally free, then $\F$ is quasi-coherent. If $U = \Spec A$, then \[\res{\F}{U} = \res{\MO_X}{U}^{\oplus I} = \widetilde{A^{\oplus I}} \,.\]
\end{remark}
\begin{remark}
	If $\F$ is locally free of constant rank $n<\infty$, then $\F$ corresponds to vector bundles of rank $n$ (\Cref{VecBunLocFr}).
\end{remark}
\begin{definition}[invertible sheaf]
	An $\MO_X$-module which is locally free of constant rank $1$ is called \textbf{invertible sheaf}.
\end{definition}
\begin{remark}[Picard group]
	We have $\mathcal L, \mathcal M$ invertible $\implies$ $\mathcal L \otimes_{\MO_X} \mathcal M$ invertible. 
	
	\textit{Proof.} We have $\res{\mathcal L}{U} \cong \res{\MO_X}{U}$ and $\res{\mathcal M}{U} \cong \res{\MO_X}{U}$ implying that $\res{\mathcal L \otimes_{\MO_X} \mathcal M}U \cong \res{\MO_X}U \otimes_{\res{\MO_X}U} \res{\MO_X}U \cong \res{\MO_X}U$.
	
	Furthermore, $\mathcal L^\vee \otimes \mathcal L \cong \Hom_{\MO_X}(\mathcal L, \mathcal L) \cong \MO_X$ (exercise).
	
	We write \[\operatorname{Pic}(X)=\{\text{invertible sheves on }X,\otimes,(-)^\vee,\MO_X \}/\text{isomorphism}\] for the \textbf{Picard group of $X$}.
\end{remark}
\begin{remark}[effective Cartier divisor]
	Let $\mathcal L$ be an invertible sheaf. Let $s \in \Gamma(X,\mathcal L)$ be a section and $x \in X$. Let $s(x)$ be the image of $s$ under \[\Gamma(X,\mathcal L) \to \mathcal L_x \to \mathcal L/(\mi_x\mathcal L_x) = \mathcal L(x)\] where $\MO_{X,x} \supset \mi_x$. 
	Let \[D(s)=\setdef{x \in X}{s(x) \neq 0} \,.\]
	Let $x \in U \subset X$ be open such that there is an isomorphism $\varphi \colon \res{\mathcal L}{U} \to \res{\MO_X}{U}$. Let $f_S = \varphi(\res{s}{U}) \in \MO_X(U)$. Consider \[\begin{tikzcd}
		\Gamma(U,\mathcal L) \arrow{r} \arrow{d}{\cong}[swap]{\res{\varphi}U} & \mathcal L_x \arrow{d}{\cong} \arrow{r} & \mathcal L(x) \arrow{d}{\varphi_x} \\
		\MO_X(U) \arrow{r} & \MO_{X,x} \arrow{r} & \kappa(x)
	\end{tikzcd}\]
	which implies that $f_s(x) = \varphi_x(s(x))$. Therefore, $D(s) \cap U = D(f_s)$.
	
	Let $$V(s) \coloneqq \setdef{x \in X}{s(x)=0}$$ which is closed with the scheme structure defined by $V(s) \cap U = V(f_s)=\Spec(A/f_s)$ if $U=\Spec A$. The set $V(s)$ is called an \textbf{effective Cartier divisor}.
\end{remark}
\section{Coherent Sheaves}
\begin{definition}[finitely generated, of finite presentation, coherent]
	An $A$-module $M$ is 
	\begin{enumerate}
		\item \textbf{finitely generated} if there exists a surjection \[\begin{tikzcd}
		A^{\oplus r} \arrow[two heads]{r} & M \arrow{r} & 0 
		\end{tikzcd}\] for some $r \ge 0$.
		\item \textbf{of finite presentation} if there exists an exact sequence \[A^{\oplus s} \longrightarrow A^{\oplus r} \longrightarrow M \longrightarrow 0\] for some $r,s \ge 0$.
		\item \textbf{coherent} is $M$ is finitely generated and for all $\varphi \colon A^{\oplus r} \to M$ we have that $\ker(\varphi)$ is finitely generated
	\end{enumerate}
\end{definition}
\begin{note}
	Free of rank $r$ does not imply coherent.
\end{note}
\begin{example}
	$A = k[x_0,x_1,x_2,\dotsc] / (x_ix_j, \, i,j \geq 1)$, $M = A$ is free.
	$\varphi \colon A \to M$, $p \mapsto x_1 \cdot p$.
	We have $\ker \varphi = \bigoplus_{i = 1}^\infty k \cdot x_i$, which is not finitely generated.
\end{example}
\begin{proposition}
	coherent $\implies$ of finite presentation $\implies$ finitely generated. 
	
	If $A$ is Noetherian, then all conditions are equivalent. 
\end{proposition}
\begin{definition}[finitely generated, of finte presentation, coherent]
	A quasi-coherent sheaf $\mathcal F$ is \textbf{finitely generated}/\textbf{of finite presentation}/\textbf{coherent} if for all affine open $U \subset X$, $\mathcal F(U)$ is finitely generated/of finite presentation/coherent.
\end{definition}
\begin{proposition}
	If $X$ is Noetherian, then finitely generated  $\iff$ finite presentation $\iff$ coherent.
\end{proposition}
\begin{proposition}
	Let $X$ be a scheme. The category $\operatorname{Coh}(X)$ of coherent sheaves is abelian.
\end{proposition}
\begin{definition}[support]
	The \textbf{support} of $\mathcal F$ is defined to be \[\operatorname{Supp}(\F)  \coloneqq \setdef{x \in X}{\mathcal F_x \neq 0} \,.\]
\end{definition}
\begin{lemma}
	If $\F$ is quasi-coherent of finite type, then $\operatorname{Supp}(\F)$ is closed. 
\end{lemma}
\begin{proof}
	Let $U$ be an open affine and let $\varphi \colon \MO_X(U)^{\oplus r} \twoheadrightarrow \F(U)$. Let $m_i=\varphi(e_i)$ where $e_i=(0,\dots , 0,1,0, \dots ,0)$. Then \[\operatorname{Supp}(\F) = \setdef{x \in X}{m_{i,x} \neq 0\text{ for some } i=1,\dots , r}\] and hence \[\operatorname{Supp}(\F) = \bigcup_{i=1}^r \operatorname{Supp}(m_i) = \bigcup_{i=1}^r V(\operatorname{Ann}(m_i))\] which is closed.
\end{proof}
\section{Quasi-coherent Sheaves on $\Proj A$}
We are going to introduce the $\sim$-construction for $\Proj$. Let $M=\bigoplus_{d \in \Z} M_d$ be a graded $A$-module (i.e. $A_d \cdot M_e \subset M_{d+e}$).

Define $\widetilde M$ on basic open subset by \[\widetilde M(D(f)) \coloneqq (M_f)_0 \,.\] If $D(g) \subset D(f)$ with $f,g \in A_+$, then $g \in \sqrt{(f)}$, so $g^n=fa$ for some $a \in A$. 
The restriction map is given by \[\begin{tikzcd}
	\widetilde M(D(f))=(M_f)_0 \arrow{d}& \frac{m}{f^k} \arrow{d} \\
	\widetilde M(D(g))=(M_g)_0 & \frac{ma^k}{f^ka^k}=\frac{ma^k}{g^{nk}}
\end{tikzcd}\]
This becomes a sheaf on the basic opens of $\Proj A$. 

\textit{Proof.} Let $D(g) \subset D(f)$ with $f,g \in A_+$. Then \[\widetilde M(D(g)) = (M_g)_0 \cong ((M_f)_0)_{\overline g} = \widetilde{(M_f)_0}(D(\overline g))\] where $\overline g = \frac{g^{\deg f}}{f^{\deg g}}$. 
Therefore, \[\res{\widetilde M}{D(f)} = \widetilde{(M_f)_0}\] which is affine, hence $\res{\widetilde M}{D(f)}$ is a sheaf on basic opens of $\Spec (A_f)$. We have \[\mathcal B = \bigcup_{f \in A_+} \mathcal B_{\Spec (A_f)_0}\] for the basic opens of $\Proj A$,. The claim follows. 

We let $\widetilde M$ be the induced sheaf on $\Proj A$ with canonical $\MO_X(U)$-module structure defined by $(A_f)_0 \times (M_f)_0 \to (M_f)_0$. 
\begin{lemma}
	$\widetilde M$ is quasi-coherent.
\end{lemma}
\begin{proof}
	By the proof above. 
\end{proof}
\begin{example}
	Let $M=A$, then $\widetilde M = \MO_{\Proj A}$ by construction
\end{example}
\begin{example}
	Let $A=\C[x,y]$ and $X=\Proj A = \PR_\C^1$. Let $M=\C[x,y]/(ax+by,x^2,xy,y^2)$. Then $\widetilde M(D(x))=(M_x)_0 = 0$ since $M_x=0$ since $x$ acts nilpotently. 
	Similarly, $\widetilde M(D(y))=0$. Therefore, $\widetilde M = 0$. 
	
	More generally, if there exists $N$ such that $M_d=0$ for all $d \ge N$, then by the same argument $\widetilde M = 0$
	
	Intuition: Let $k=\C$ and $A=\C[x,y]$. We can think of $\PR_\C^1$ as $(\C^2 \setminus \{0\}) / \C^\ast$. If $M_d=0$ for all $d \ge N$, then $\mi^N \cdot M = 0$ with $\mi=(x,y)$. $M$ is supported at $(0,0)$. 
	
	Before quotiening we take out $(0,0)$, so loose information about $M$.
	
	Another intuition: There is an isomorphism \begin{align*}
		\text{graded $A$-modules} & \longrightarrow \text{$A$-modules with $\C^\ast$-action} \\
		M=\bigoplus_{d \in \Z} M_d & \longmapsto (M, t \text{ acts on $M_d$ by $t^d$})
	\end{align*} 
	Now, $M_0$ is invariant of $\C^\ast$-action and so is $(M_f)_0$. 
\end{example}
\begin{example}
	Let $A$ as before and $M=\C[y]=\C[x,y]/(x)$. Here we have \[\widetilde M(D(x))=(M_x)_0=0\] and \[\widetilde M(D(y))=(\C[y]_{(y)})_0=\C \,.\] Therefore, $\widetilde M$ is the skyscraper sheaf on $\PR_\C^1$ at $[x,y]=[0,1]$. 
\end{example}
\begin{lemma}
	The functor $\operatorname{Graded }A\operatorname{-Mod} \to \operatorname{QCoh}(\Proj A)$ defined by
	\begin{align*}
		M & \longmapsto \widetilde M \\
		(u \colon M \to N) & \longmapsto (\widetilde u \colon \widetilde M \to \widetilde N)
	\end{align*}
	is exact (where $\widetilde u$ is defined by $(\widetilde u_f)_0 \colon (M_f)_0 \to (N_f)_0$).
\end{lemma}
\begin{proof}
	Let $M \to N \to P$ be exact. Then $M_f \to N_f \to P_f$ is exact and so is $(M_f)_0 \to (N_f)_0 \to (P_f)_0$. Therefore, $\widetilde{(M_f)_0} \to \widetilde{(N_f)_0} \to \widetilde{(P_f)_0}$ is exact (since affine $\sim$ are exact). This finally shows that $\widetilde M \to \widetilde N \to \widetilde P$ is exact. 
\end{proof}
\begin{example}
	Let $I=(x)$. Then $0 \to I \to \C[x,y] \to \C[y] \to 0$ is exact and so is $0 \to \widetilde I \to \MO_X \to \C_{[0,1]} \to 0$. 
\end{example}
%\section{Serre Twisting Sheaf}
Let $M$ be a graded module and $M(n) = \bigoplus_{d \in \Z} M(n)_d$ where $M(n)_d \coloneqq M_{n+d}$. 
\begin{example}
	Let $A=\C[x,y]$ and $M=(x)$. Then $M_0=0=A_{-1}$ and $M_1=\C=A_0$ and $M_2=\C\langle x,y \rangle = A_1$ and so on. We have $M \cong A(-1)$ 
\end{example}
\begin{definition} Let $\MO_X(n) = \widetilde{A(n)}$
\end{definition}
On $D(f)$ we have that \[\MO_X(n)(D(f))=(A(n)_f)_0=(A_f)_n \,.\] If $\deg f | n$, then 
\begin{align*}
	(A_f)_n & \longrightarrow (A_f)_0 \\
	\frac{a}{f^k} & \longmapsto \frac{a}{f^kf^{n/\deg (f)}}
\end{align*}
is an isomorphism. Therefore, \[\res{\MO_X(n)}{D(f)} \cong \widetilde{(A_f)_n} \cong \widetilde{(A_f)_0} = \res{\MO_X}{D(f)} \,.\]
\begin{proposition}
	Assume that $A$ is a graded ring such that $A$ is generated by $A_1$ over $A_0$. Then 
	\begin{enumerate}[label=(\alph*)]
		\item $\MO_X(n)$ is an invertible sheaf for all $n$.
		\item For any graded $A$-module $M$ we have \[\widetilde{M(n)} \cong \widetilde M \otimes_{\MO_X} \MO_X(n) \,.\]
		In particular $\MO_X(m) \otimes \MO_X(n) = \MO_X(m+n)$.
		\item $\MO_X(n)^\vee = \MO_X(-n)$
	\end{enumerate}
\end{proposition}
\begin{remark}
	For an $\MO_X$-module $\F$ we define \[\F(n) \coloneqq \F \otimes_{\MO_X} \MO_X(n) \,.\] Then $(b)$ says that $\widetilde M(n) = \widetilde{M(n)}$. 
\end{remark}
\begin{remark}
	The statements $(b),(c)$ lead to a group homomorphism 
	\begin{align*}
		\Z & \longrightarrow \operatorname{Pic}(X) \\
		n & \longmapsto \MO_X(n)
	\end{align*}
	Later, we will see that this in fact an isomorphism.
\end{remark}
\begin{proof}~ \vspace{-\topsep}
	\begin{enumerate}
		\item Let $A$ be generated by $A_1$. Then, $\Proj A = \bigcup_{f \in A_1} D(f)$ as \[\left(\bigcup_{f \in A_1} D(f) \right)^C = V(A_1) = V(A) \,.\] Furthermore by $1=\deg(f)|n$ we have \[\res{\MO_X(n)}{D(f)}\cong \res{\MO_X}{D(f)}\] and we get that $\MO_X(n)$ is locally free of rank $1$.\qedhere
	\end{enumerate}
\end{proof}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "alggeoI_main"
%%% End:
